create database DB_Sales

use DB_Sales

create table salesperson(
ID int primary key not null,
NAME varchar(50) not null,
BOD date not null,
SALARY decimal(18,2) not null
)

insert into salesperson( ID, NAME, BOD, SALARY)
values
(1,'Abe','1988/11/9',140000),
(2,'Bob','1978/11/9',44000),
(3,'Chris','1983/11/9',40000),
(4,'Dan','1980/11/9',52000),
(5,'Ken','1977/11/9',115000),
(6,'Joe','1990/11/9',38000)

create table orders(
ORDER_DATE date not null,
CUST_ID int not null,
SALESPERSON_ID int not null,
AMOUNT decimal (18,2) not null
)

insert into orders( ORDER_DATE, CUST_ID, SALESPERSON_ID, AMOUNT)
values
('2020/8/2',4,2,540),
('2021/1/22',4,5,1800),
('2019/7/14',9,1,460),
('2018/1/29',7,2,2400),
('2021/2/3',6,4,600),
('2020/3/2',6,4,720),
('2021/5/6',9,4,150)

select * from orders
select * from salesperson

--soal a (informasi nama sales yg memiliki order lebih dari 1)
select salesperson.NAME
from salesperson
join orders on salesperson.ID = orders.SALESPERSON_ID
group by salesperson.NAME
having COUNT(orders.SALESPERSON_ID) > 1

--soal b (nama sales dengan total amount diatas 1000)

--soal d ( rata2 salary dari tertinggi)
select top 1 salesperson.NAME , avg(orders.amount)
from salesperson
join orders on salesperson.ID = orders.SALESPERSON_ID
group by salesperson.NAME
order by avg(orders.amount) desc




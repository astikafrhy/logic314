create database DB_KisiKisi

use DB_KisiKisi

create table assigment(
id int primary key identity (1,1),
name varchar(50) not null,
marks int not null,
)

insert into assigment(name,marks)
values
('Isni',85),
('Laudry',75),
('Bambang',40),
('Anwar',91),
('Alwi',70),
('Fulan',50)

select name, marks,
case 
when marks > 90 then 'A+' 
when marks > 70 then 'A'
when marks > 60 then 'B'
when marks > 40 then 'C'
else 'FAIL'
end
as [grade]
from assigment

----------------------------------------------
create table employee (
id int primary key identity (1,1),
name varchar(50) not null
)

insert into employee(name)
values
('Isni'),
('Laudry'),
('Firdha'),
('Astika')

create table department (
id int primary key identity (1,1),
name varchar(50) not null,
salary decimal (18,2) not null,
employee_id int not null
)

alter table department drop column employee_id 
alter table department drop column salary

alter table employee add salary decimal (18,2)

select * from employee
select * from department

insert into department(name)
values
('HRD'),
('Accounting'),
('IT'),
('Finance')

update employee set employee_id=1 , salary=100000 where id =1
update employee set employee_id=2 , salary=50000 where id =2
update employee set employee_id=3 , salary=200000 where id =3
update employee set employee_id=4 , salary=250000 where id =4

select top 1 department.name , avg(employee.salary)
from employee
join department on employee.employee_id= department.id
group by department.name 
order by avg(employee.salary) desc

select*from employee
select*from department







-- TUGAS SQL DAY 02 --

create database DB_Entertainer

use DB_Entertainer

create table artis(
kd_artis varchar(100) primary key not null,
nm_artis varchar(100) not null,
jk varchar(100) not null,
bayaran bigint not null,
award int not null,
negara varchar(100) not null
)
select * from artis

insert into artis(kd_artis,nm_artis,jk,bayaran,award,negara)
values
('A001','ROBERT DOWNEY JR','PRIA',3000000000,2,'AS'),
('A002','ANGELINA JOLIE','WANITA',700000000,1,'AS'),
('A003','JACKIE CHAN','PRIA',200000000,7,'HK'),
('A004','JOE TASLIN','PRIA',3500000000,1,'ID'),
('A005','CHELSEA ISLAN','WANITA',300000000,0,'ID')

ALTER table 

select * from artis

create table film(
kd_film varchar(10) primary key not null,
nm_film varchar(55) not null,
genre varchar (55) not null,
artis varchar (55) not null,
prosedur varchar (55) not null,
pendapatan int not null,
nominasi int not null
)
alter table film alter column pendapatan bigint not null


insert into film(kd_film,nm_film,genre,artis,prosedur,pendapatan,nominasi)
values
('F001','IRON MAN','G001','A001','PD01',2000000000,3),
('F002','IRON MAN 2','G001','A001','PD01',1800000000,2),
('F003','IRON MAN 3','G001','A001','PD01',1200000000,0),
('F004','AVENGER: CIVIL WAR','G001','A001','PD01',2000000000,1),
('F005','SPIDERMAN HOME COMING','G001','A001','PD01',1300000000,0),
('F006','THE RAID','G001','A004','PD03',800000000,5),
('F007','FAST & FURIOUS','G001','A004','PD05',300000000,2),
('F008','HABIBIE DAN AINUN','G004','A005','PD03',670000000, 4),
('F009','POLICE STORY','G001','A003','PD02',700000000,3),
('F010','POLICE STORY 2','G001','A003','PD02',710000000,1),
('F011','POLICE STORY 3','G001','A003','PD02',615000000,0),
('F012','RUSH HOUR','G003','A003','PD05',695000000,2),
('F013','KUNGFU FANDA','G003','A003','PD05',923000000,5)

select * from film

create table produser(
kd_producer varchar(50) primary key not null,
nm_producer varchar(50) not null,
international varchar(50) not null
)

insert into produser(kd_producer,nm_producer,international)
values
('PD01','MARVEL','YA'),
('PD02','HONGKONG CINEMA','YA'),
('PD03','RAFI FILM','TIDAK'),
('PD04','PARKIT','TIDAK'),
('PD05','PARAMOUNT PICTURE','YA')

SELECT * FROM produser

create table negara(
kd_negara varchar(100) primary key not null,
nm_negara varchar(100) not null
)

insert into NEGARA(kd_negara, nm_negara)
values
('AS','AMERIKA SERIKAT'),
('HK','HONGKONG'),
('ID','INDONESIA'),
('IN','INDIA')
SELECT * FROM NEGARA

CREATE table genre(
kd_genre varchar(50) primary key not null,
nm_genre varchar(50) not null
)

insert into genre(kd_genre,nm_genre)
values
('G001', 'ACTION'),
('G002', 'HORROR'),
('G003', 'COMEDY'),
('G004', 'DRAMA'),
('G005', 'THRILLER'),
('G006', 'FICTION')

SELECT * FROM GENRE

-------------------------------
--SOAL 1 (jumlah pendapatan produser marvel) // revisi
select pr.nm_producer ,sum(fl.pendapatan) as jumlah_pendapatan
from film as fl
join produser as pr 
on fl.prosedur = pr.kd_producer
where pr.nm_producer='marvel'
group by pr.nm_producer

--SOAL2(nama film dan nominasi yg tdk dapat nominasi)
select nm_film, nominasi from film where nominasi=0

--soal 3 (menampilkan nama film huruf depan p)
select nm_film from film where nm_film like 'p%'

--soal 4 (menampilkan nama film yang huruf terakhitnya y)
select nm_film from film where nm_film like '%y'

--soal 5 (nama film yang mengandung huruf d)
select nm_film from film where nm_film like '%d%'

--soal6 (nama film dan artis)
select film.nm_film, artis.nm_artis 
from film
join artis
on film.artis = artis.kd_artis
group by film.nm_film , artis.nm_artis

--soal 7 (nama file yang artisnya berasal dari hongkong)
select film.nm_film, artis.negara 
from film
join artis
on film.artis = artis.kd_artis
where artis.negara = 'hk'

--soal 8 (nama film yg artisnya bukan dari negara yg ada huruf o)
select film.nm_film , negara.nm_negara
from film
join artis on film.artis= artis.kd_artis
join negara on  artis.negara = negara.kd_negara
where negara.nm_negara not like '%o%'

--soal 9(nama artis yang tidak pernah bermain genre drama)

select artis.nm_artis
from artis
left join film on artis.kd_artis = film.artis
--join genre on film.genre = genre.kd_genre
where film.nm_film is null

--soal 10(nama artis yang bermain dengan genre drama)
select artis.nm_artis , genre.nm_genre
from artis
left join film on artis.kd_artis = film.artis
join genre on film.genre = genre.kd_genre
where genre.nm_genre = 'DRAMA'

--soal 11(nama artis bergenre action)
select artis.nm_artis , genre.nm_genre
from artis
join film on artis.kd_artis = film.artis
join genre on film.genre = genre.kd_genre
where genre.nm_genre = 'action'

--soal 12(data negara dengan jumlah filmnya)
select  artis.negara, negara.nm_negara, count(negara.nm_negara) as jumlahFilm
from film
join artis on film.artis = artis.kd_artis
join negara on artis.negara = negara.kd_negara
group by negara.nm_negara , artis.negara

select * from artis
select * from film
select * from produser

--soal13(nama film yg skala internasional)
select film.nm_film
from film
join produser on film.prosedur = produser.kd_producer
--join negara on artis.negara = negara.kd_negara
where produser.international = 'YA'

--soal 14 (jumlah film dari masing2 produser)
select produser.nm_producer, count(film.nm_film)
from film
join produser
on film.prosedur = produser.kd_producer
group by produser.nm_producer





--DDL--
-- CREAT DATABASE
create database db_kampus

use db_kampus

--CREATE TABLE
create table mahasiswa(
id bigint primary key identity (1,1),
nama varchar(50) not null,
alamat varchar(50) not null,
email varchar(255) null
)

--CREATE VIEW
create view vwMahasiswa
as 
select id,nama,alamat,email from mahasiswa

--SELECT VIEW
select * from vwMahasiswa


---------------------------------------------------
--ALTER ADD COLUMN
alter table mahasiswa add deskripsi varchar (255)

--ALTER DROP COLUMN
alter table mahasiswa drop column deskripsi

--TABLE ALTER COLUMN 
alter table mahasiswa alter column email varchar(100)

--ALTER VIEW
--alter view [NamaViewLama] to [NamaViewBaru]


----------------------------------------------------
--DROP DATABASE
drop database db_kampus2

--DROP TABLE
drop table mahasiswa2

--DROP VIEW
drop view vwMahasiswa 


--DML--
--INSERT DATA
insert into mahasiswa(nama,alamat,email) 
values 
('Astika', 'Garut2', 'astikafr17@gmail.com'),
('Toni', 'Garut', 'toni@gmail.com'),
('Isni', 'Cimahi', 'isni@gmail.com'),
('lala', 'bogor', 'lala@gmail.com'), 
('maya', 'bandung', 'maya@gmail.com')

alter table mahasiswa add panjang smallint


--SELECT DATA
select id,nama,alamat,email from mahasiswa
select * from mahasiswa

--UPDATE DATA
update mahasiswa set nama='Firda',alamat='Tangsel',email='firda@gmail.com' where id='4'

update mahasiswa set panjang='48' where id='1'
update mahasiswa set panjang='86' where id='2'
update mahasiswa set panjang='117' where id='3'
update mahasiswa set panjang='100' where id='4'
update mahasiswa set panjang='50' where id='6'

select * from mahasiswa

--DELETE DATA
delete from mahasiswa where id='5'

--JOIN DATA
select mhs.id, nama, email, mahasiswa_id, year(bio.dob) as tahunLahir
from mahasiswa as mhs
join biodata as bio on mhs.id = bio.mahasiswa_id
where mhs.nama = 'toni' or /* atau and atau not */ bio.kota = 'jakarta'

-- ORDER BY
select * 
from mahasiswa 
join biodata on mahasiswa.id = biodata.mahasiswa_id
order by /*mahasiswa.id asc,*/ mahasiswa.nama desc

--SELECT TOP 
select top 2 * from mahasiswa order by nama desc

--BETWEEN 
select * from mahasiswa where id between 1 and 3
select * from mahasiswa where id >= 1 and id <=3

--LIKE
select * from mahasiswa where nama like 'a%'
select * from mahasiswa where nama like '%i'
select * from mahasiswa where nama like '%on%'
select * from mahasiswa where nama like '_i%'
select * from mahasiswa where nama like '__t%'
select * from mahasiswa where nama like 'a_%'
select * from mahasiswa where nama like 'f__%'
select * from mahasiswa where nama like 'i%i'

--GROUP BY
select nama from mahasiswa group by nama
select count(id),nama from mahasiswa group by nama

-- HAVING dan COUNT
select count(id),nama 
from mahasiswa 
group by nama 
having  count(id)>1

--MIN


--MAX

--DISTINCT
select distinct nama from mahasiswa

--SUBSTRING
select SUBSTRING ('SQL Tutorial', 1,3) as Judul

--CHARINDEX
select charindex ('t', 'customer') as judul

--DATA LENGTH
select DATALENGTH('akusiapayages') as Judul

--CASE when
select id, nama, alamat,email,panjang,
	case 
	when panjang < 50 then'Pendek'
	when panjang <= 100 then'Sedang'
	else 'Tinggi'
	end as tinggiBadan
from mahasiswa 

--CONCAT
select concat('SQL', ' Is ', 'Fun!')
select 'SQL' + ' Is ' + 'Fun!'

------------------------------------------------------
--OPERATOR ARITMATIKA

------------------------------------------------------
create table biodata(
id bigint primary key identity (1,1),
dob datetime not null,
kota varchar(100) null
)

alter table biodata add mahasiswa_id bigint
alter table biodata alter column mahasiswa_id bigint not null

alter table mahasiswa add panjang smallint

alter table biodata alter column dob date not null

insert into biodata(dob,kota,mahasiswa_id)
values
('1999/09/17', 'Jakarta', 1),
('1998/08/05', 'Jakarta', 2),
('2000/06/26', 'Jakarta', 3),
('1998/01/01', 'Jakarta', 4)

select * from biodata

--------------------------------------------------------
create table penjualan(
nama varchar(50) not null,
harga int not null
)
alter table penjualan add id bigint primary key identity(1,1)

insert into penjualan(nama,harga)
values
('Indomie', 1500),
('Close-Up', 3500),
('Pepsoden', 3000),
('Brush Formula', 2500),
('Roti Manis', 1000),
('Gula', 3500),
('Sarden', 4500),
('Rokok Sampurna', 11000),
('Rokok 234', 11000)

select * from penjualan

select nama, harga, 
harga * 100 as [harga * 100] 
from penjualan 
----------------------------------------------------------------

use DBPenerbit

create table tblPengarang(
iD int primary key not null,
Kd_Pengarang varchar(7) not null,
Nama varchar(30) not null,
Alamat varchar(80) not null,
Kota varchar(15) not null,
Kelamin varchar(1) not null
)

insert into tblPengarang(iD,Kd_Pengarang,Nama,Alamat,Kota,Kelamin)
values
(1, 'P0001','Ashadi', 'Jl. Beo 25', 'Yogya', 'P'),
(2, 'P0002','Rian', 'Jl. Solo 123', 'Yogya', 'P'),
(3, 'P0003','Suwadi', 'Jl. Semangka 13', 'Bandung', 'P'),
(4, 'P0004','Siti', 'Jl Durian 15', 'Solo', 'W'),
(5, 'P0005','Amir', 'Jl. Gajah 33', 'Kudus', 'P'),
(6, 'P0006','Suparman', 'Jl. Harimau 25', 'Jakarta', 'P'),
(7, 'P0007','Jaja', 'Jl. Singa 7', 'Bandung', 'P'),
(8, 'P0008','Saman', 'Jl. Naga 12', 'Yogya', 'P'),
(9, 'P0009','Anwar', 'Jl. Tidar 6A', 'Magelang', 'P'),
(10, 'P0010','Fatmawati', 'Jl. Renjana 4', 'Bogor', 'W')

create table tblGaji(
ID int primary key not null,
Kd_Pengarang varchar(7) not null,
Nama varchar(30) not null,
Gaji decimal(18,4) not null
)

insert into tblGaji(ID,Kd_Pengarang,Nama,Gaji)
values
(1,'P0002', 'Rian', '600000'),
(2,'P0005', 'Amir', '700000'),
(3,'P0004', 'Siti', '500000'),
(4,'P0003', 'Suwadi', '1000000'),
(5,'P0010', 'Fatmawati', '600000'),
(6,'P0008', 'Saman', '750000')

--SOAL 1 (tampilkan nama pengarang)
select count(Nama) as jumlah, Nama from tblPengarang group by Nama
select count(Nama) as jumlah_nama from tblPengarang

--SOAL 2 (hitung jenis kelamin w dan p)
select count(iD) as jumlah,Kelamin from tblPengarang group by Kelamin 
select Kelamin, count(iD) as jumlah from tblPengarang group by Kelamin 

--SOAL 3 ( hitung jumlah kota)
select COUNT(iD) as jumlah_kota, Kota from tblPengarang group by Kota

--SOAL 4 (lebih dari satu kota)
select count(iD) as jumlah_kota,Kota
from tblPengarang
group by Kota
having  count(iD)>1

--SOAL 5 (max min dari kd_pengarang di tblPengarang)
--cara 1
select MAX(Kd_Pengarang) as max_Kd_Pengarang, min(Kd_Pengarang) as min_Kd_Pengarang
from tblPengarang 

--cara2
select
(select top 1 Kd_Pengarang from tblPengarang order by Kd_Pengarang desc) as max_kd_pengarang, 
(select top 1 Kd_Pengarang from tblPengarang order by Kd_Pengarang asc) as min_kd_pengarang


--cara3
select max (Kd_Pengarang) as max
from tblPengarang
select MIN(Kd_Pengarang) as min
from tblPengarang 

--SOAL 6 (gaji tertinggi dan terendah)
select max(Gaji) as max_gaji, min(Gaji) as min_gaji
from tblGaji

select max(Gaji) as max
from tblGaji
select min(Gaji) as min
from tblGaji

--SOAL 7 (Tampilkan gaji diatas 600rb)

select Gaji from tblGaji where gaji >600000

-- SOAL 8 (tampilkan jumlah gaji) 
select sum(Gaji) from tblGaji

--SOAL 9 (jumlah gaji berdasarkan kota)
select pengarang.Kota, sum(Gaji) as jumlahGaji
from tblGaji as gaji
join tblPengarang as pengarang
on pengarang.Kd_Pengarang = gaji.Kd_Pengarang
group by pengarang.Kota

--SOAL 10 (tampilkan data dari p0003-p0006)
--cara 1
select * from tblPengarang where Kd_Pengarang >= 'P0003' and Kd_Pengarang <= 'P0006'
--cara 2
select * from tblPengarang where Kd_Pengarang between 'P0003' and 'P0006'

--SOAL 11 (tampilkan data hanya kota yogya, solo, magelang)
--cara 1
select * from tblPengarang where Kota = 'Yogya'or Kota = 'Solo' or Kota = 'Magelang'
--cara 2
select * from tblPengarang where Kota in('yogya', 'solo', 'magelang')

--SOAL 12 (bukan yogya)
select * from tblPengarang where Kota != 'Yogya'
select * from tblPengarang where Kota <> 'Yogya'
select * from tblPengarang where Kota not like 'Yogya'

--SOAL 13
/* a. (dimulai dengan huruf a)*/ select * from tblPengarang where Nama like 'a%'
/* b. (berakhiran dengan huruf i)*/ select * from tblPengarang where Nama like '%i'

/* - tambahan - tidak berakhiran angka 4 dan berakhiran huruf i */ 
select * from tblGaji where Kd_Pengarang not like '%4' and nama like '%i'

/* c. (angka ke3nya a)*/ select * from tblPengarang where Nama like '__a%'
/* d. (tidak berakhiran n)*/ select * from tblPengarang where Nama  not like '%n'

--SOAL 14 (tampilkan tabel di (tblpengarang+tblgaji) pake kd_pengarang)
select pengarang.Kd_Pengarang, pengarang.Nama, pengarang.Alamat, pengarang.Kota, pengarang.Kelamin, gaji.Gaji
from tblPengarang as pengarang 
inner join tblGaji as gaji 
on pengarang.Kd_Pengarang = gaji.Kd_Pengarang

select *
from tblPengarang as pengarang 
join tblGaji as gaji 
on pengarang.Kd_Pengarang = gaji.Kd_Pengarang

--SOAL 15 (gaji dibawah 1jt menurut kota)
select pengarang.Kota, gaji.Gaji
from tblPengarang as pengarang 
join tblGaji as gaji 
on pengarang.Kd_Pengarang = gaji.Kd_Pengarang
where gaji > 0 and gaji < 1000000

--SOAL 16 (panjang jadi 10)
alter table tblPengarang alter column Kelamin varchar(10)

--SOAL 17 (tambah gelar  tipe varchar (12))
alter table tblPengarang add Gelar varchar(12) 
select * from tblPengarang

--SOAL 18 (ubat alamat dan kota dari Rian)
update tblPengarang set alamat='Jl. Cendrawasih 65', Kota='Pekanbaru' where Nama = 'Rian'
select * from tblPengarang where Nama = 'Rian'

-- SOAL 19 (buat view kd_pengarang, nama, kota, gaji dengan nama vwPengarang)
create view vwPengarang
as
select pengarang.Kd_Pengarang, pengarang.Nama, pengarang.Kota , gaji.Gaji
from tblPengarang as pengarang 
join tblGaji as gaji
on pengarang.Kd_Pengarang = gaji.Kd_Pengarang

select * from vwPengarAng
---------------------------------------------------------------------

﻿using System;
using System.Collections.Generic;

namespace Logic07
{
    class Mamalia
    {
        virtual public void pindah()
        {
            Console.WriteLine("Kucing Lari....");
        }
    }

    class Kucing : Mamalia
    {
        
    }

    class Paus : Mamalia
    {
        public override void pindah()
        {
            Console.WriteLine("Paus Berenang");
        }
    }
    public class Program
    {
        static List<User> listUser = new List<User>();
        static void Main(string[] args)
        {

            //bool ulangi = true;
            //while (ulangi)
            //{
               // tambahUser();
            //}

            //listUser();
            ///dateTime();
            stringDatetime();
            //timeSpan();
            //class_inheritance();
            //overriding();


            Console.ReadKey();
        }
        static void overriding()
        {
            Paus paus = new Paus();
            paus.pindah();

            Kucing kucing = new Kucing();
            kucing.pindah();
        }
        static void class_inheritance()
        {
            TypeMobil typeMobil = new TypeMobil();
            typeMobil.kecepatan = 100;
            typeMobil.posisi = 50;
            typeMobil.Civic();
        }
        static void timeSpan()
        {
            DateTime date1 = new DateTime(2023, 1, 1, 0, 0, 0);
            DateTime date2 = DateTime.Now;

            //TimeSpan span = date1.Subtract(date2);
            TimeSpan span = date2 - date1;

            Console.WriteLine($"Hari        : {span.Days}");
            Console.WriteLine($"Total Hari  : {span.TotalDays}");
                
            Console.WriteLine($"Jam         : {span.Hours}");
            Console.WriteLine($"Total Jam   : {span.TotalHours}");

            Console.WriteLine($"Menit       : {span.Minutes}");
            Console.WriteLine($"Total Menit : {span.TotalMinutes}");

            Console.WriteLine($"Detik       : {span.Seconds}");
            Console.WriteLine($"Total Detik : {span.TotalSeconds}");

        }
        static void stringDatetime()
        {
            Console.WriteLine("==== String DateTime ====");
            Console.Write("Masukkan tanggal (dd/mm/yyy) : ");
            string strTanggal = Console.ReadLine();

            DateTime tanggal = DateTime.Parse(strTanggal);

            Console.Write($"Tgl : {tanggal.Day}");
            Console.Write($"Bulan : {tanggal.Month}");
            Console.Write($"Tahun : {tanggal.Year}");
            Console.Write($"DayOfWeek ke : {(int)tanggal.DayOfWeek}");
            Console.WriteLine($"DayOfWeek ke : {tanggal.DayOfWeek}");
        }
        static void dateTime()
        {
            DateTime dt1 = new DateTime(); // 01/01/0001 00:00:00
            Console.WriteLine(dt1);

            DateTime dt2 = DateTime.Now; //waktu sekarang
            Console.WriteLine(dt2);

            DateTime d3 = DateTime.Now.Date; //tanggal sekarang tanpa time
            Console.WriteLine(d3);

            DateTime dt4 = DateTime.UtcNow;
            Console.WriteLine(dt4);

            DateTime dt5 = new DateTime(2023, 03, 09);
            Console.WriteLine(dt5);

            DateTime dt6 = new DateTime(2023, 3, 9, 11, 45, 00);
            Console.WriteLine(dt6);
        }
        static void tambahUser()
        {
            

            Console.Write("Masukkan No\t : ");
            int Nomor = int.Parse(Console.ReadLine());
                
            Console.Write("Masukkan Nama\t: ");
            string nama = Console.ReadLine();

            Console.Write("Masukkan Umur\t: ");
            int umur = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Alamat\t: ");
            string Alamat = Console.ReadLine();

            Console.WriteLine("---------------------------------------------");
            Console.WriteLine("================ DAFTAR USER ================");
            Console.WriteLine("---------------------------------------------");
            Console.WriteLine("No" + "\t" + "Nama" + "\t" + "Umur" + "\t" + "Alamat");
            Console.WriteLine("---------------------------------------------");


            User user = new User();
            user.Nomor = Nomor;
            user.Nama = nama;
            user.Umur = umur;
            user.Alamat = Alamat;
             
            listUser.Add(user);

            for (int i = 0; i < listUser.Count; i++)
                Console.WriteLine($"{listUser[i].Nomor}\t {listUser[i].Nama}\t {listUser[i].Umur}\t {listUser[i].Alamat}");
        }
        static void listUsers()
        {
            List<User> listUser = new List<User>()
            {
                new User(){Nama = "Firdha", Umur = 24, Alamat = "Tangsel"},
                new User(){Nama = "Isni", Umur = 22, Alamat = "Cimahi"},
                new User(){Nama = "Asti", Umur = 23, Alamat = "Garut"},
                new User(){Nama = "Muafa", Umur = 22, Alamat = "Bogor"},
                new User(){Nama = "Toni", Umur = 24, Alamat = "Garut"}
            };
            for (int i = 0; i < listUser.Count; i++)
                Console.WriteLine($"Nama : {listUser[i].Nama}\tUmur :{listUser[i].Umur}\tAlamat :{listUser[i].Alamat}");
        }
    }
}
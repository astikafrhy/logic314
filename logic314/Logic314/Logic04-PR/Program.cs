﻿using System;

namespace Logic04_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Soal1_GajiKaryawan();
            //Soal2_Array();
            //Soal3_gantiHuruf();
            //Soal4_gantiHurufTengah();
            Soal5_HilangKata();  
            //Soal6_deretPangkat(); 
            //Soal7_deretAngka();
            //Soal8_Fibonacci();
            //Soal9_ubahWaktu(); 
            //Soal10_IMPFashion();
            //soal11_belanja();
            //Soal12_bintang();
            //Soal12();
            //soal10();

        }
        static void Soal12()
        {
            Console.WriteLine("==== SOAL 12 ====");
            Console.Write("masukkan elemen matriks :");
            int n = int.Parse(Console.ReadLine());
            //int a = 1, b = elemen;

            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j < n; j++)
                {

                    if (i == 1 || i == n)
                    {
                        if (i == n)
                        {
                            Console.Write(n - j + 1 + "\t");
                        }
                        else
                        {
                            Console.Write(j + "\t");
                        }

                    }
                    else
                    {
                        if (j == 1 || j == n)
                            Console.Write("*\t");
                        else
                          Console.Write(" \t");
                    }
                }
                Console.WriteLine();
            }
        }
            static void Soal12_bintang()
        {
            Console.WriteLine("==== SOAL 12 ====");
            Console.Write("masukkan elemen matriks :");
            int elemen = int.Parse(Console.ReadLine());
            int a = 1, b = elemen;

            for (int i = 0; i < elemen; i++)
            {
                for (int j = 0; j < elemen; j++)
                {

                    if (i == 0)
                    {
                        Console.Write((a++).ToString() + "\t");

                    }
                    else if (i == elemen - 1)
                    {
                        Console.Write((b--).ToString() + "\t");
                    }
                    else if (j == 0 || j == elemen - 1)
                    {

                        Console.Write("*\t");
                    }
                    else
                    {
                        Console.Write('\t');
                    }
                }
                Console.WriteLine();

            }

        }
        static void soal11_belanja()
        {
            int uang;
            Console.WriteLine("==== SOAL 11 ANDI BELANJA ====");
            Console.Write("Uang Andi                             : ");
            uang = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Harga Baju (35, 40, 50, 20)  : ");
            string[] hargaBaju = Console.ReadLine().Split(',');

            Console.Write("Masukkan Harga Celana (40, 30, 45, 10): ");
            string[] hargaCelana = Console.ReadLine().Split(',');

            int[] totalBelanja = new int[hargaBaju.Length];
            int maks = 0;
            Console.WriteLine($"Andi menghabiskan uang sebesar = ");

            for (int i = 0; i < hargaBaju.Length; i++)
            {
                totalBelanja[i] = int.Parse(hargaBaju[i]) + int.Parse(hargaCelana[i]);
                Console.WriteLine(hargaBaju[i] + "+" + hargaCelana[i] + "=" + totalBelanja[i]);
            }

            for (int i = 0; i < totalBelanja.Length; i++)
            {
                for (int j = 0; j < totalBelanja.Length; j++)
                {
                    if (totalBelanja[i] > totalBelanja[j])
                    {
                        maks = totalBelanja[i];
                        totalBelanja[i] = totalBelanja[j];
                        totalBelanja[i] = maks;
                    }
                }
            }

            for (int i = 0; i < hargaBaju.Length; i++)
            {
                if (totalBelanja[i] <= uang) ;
                {
                    maks = totalBelanja[i];
                    break;
                }

            }
            Console.WriteLine($"Kamu membeli baju dan celana seharga : {maks}");

        }
        static void soal10()
        {
            string kodeBaju;
            string kodeUkuran, merk="";
            int harga = 0;

            Console.WriteLine("==== SOAL 10 IMP FASHION ====");
            Console.Write("Masukkan Kode Baju   = ");
            kodeBaju = Console.ReadLine();
            Console.Write("Masukkan kode Ukuran = ");
            kodeUkuran = Console.ReadLine().ToUpper();

            if (kodeBaju == "1")
            {
                merk = "IMP";
                switch (kodeUkuran)
                {
                    case "S":
                        harga = 200000;
                        break;
                    case "M":
                        harga = 220000;
                        break;
                    default:
                        harga = 250000;
                        break;
                }
            }
            else if (kodeBaju == "2")
            {
                merk = "Prada";
                switch (kodeUkuran)
                {
                    case "S":
                       harga = 150000; 
                        break;
                    case "M":
                        harga = 160000;
                        break;
                    default:
                        harga = 170000;
                        break;
                }
            }
            else if (kodeBaju == "3")
            {
                merk = "Gucci";
                switch (kodeUkuran)
                {
                    case "S":
                        harga = 200000;
                        break;
                    case "M":
                        harga = 200000;
                        break;
                    default:
                        harga = 200000;
                        break;
                }
            }
            else
            {
                Console.WriteLine("Data yang anda masukkan salah!");
            }
            Console.WriteLine($"Merk Baju : {merk}");
            Console.WriteLine($"Harga : {harga}");
        }
        static void Soal10_IMPFashion()
        {
            int kodeBaju;
            string kodeUkuran;

            Console.WriteLine("==== SOAL 10 IMP FASHION ====");
            Console.Write("Masukkan Kode Baju   = ");
            kodeBaju = int.Parse(Console.ReadLine());
            Console.Write("Masukkan kode Ukuran = ");
            kodeUkuran = Console.ReadLine().ToUpper();
            string kodebajuBaru = kodeBaju.ToString();


            if (kodeBaju == 1)
            {
                Console.WriteLine("Merk Baju     = IMP");
                switch (kodeUkuran)
                {
                    case "S":
                        Console.WriteLine("Harga = 200000");
                        break;
                    case "M":
                        Console.WriteLine("Harga = 220000");
                        break;
                    default:
                        Console.WriteLine("Harga = 250000");
                        break;
                }
            }
            else if (kodeBaju == 2)
            {
                Console.WriteLine("Merk Baju = Prada");
                switch (kodeUkuran)
                {
                    case "S":
                        Console.WriteLine("Harga = 150000");
                        break;
                    case "M":
                        Console.WriteLine("Harga = 160000");
                        break;
                    default:
                        Console.WriteLine("Harga = 170000");
                        break;
                }
            }
            else if (kodeBaju == 3)
            {
                Console.WriteLine("Merk Baju    = Gucci");
                switch (kodeUkuran)
                {
                    case "S":
                        Console.WriteLine("Harga = 200000");
                        break;
                    case "M":
                        Console.WriteLine("Harga = 200000");
                        break;
                    default:
                        Console.WriteLine("Harga = 200000");
                        break;
                }
            }
            else if (kodeBaju > 3)
            {
                Console.WriteLine("Data yang anda masukkan salah!");
            }
        }
        static void Soal9_ubahWaktu()
        {
            int jam;
            string menit, detik;
            string waktu;
           
            Console.WriteLine(" SOAL 9 UBAH WAKTU");

            Console.Write(" jam     : ");
            jam = int.Parse(Console.ReadLine());

            Console.Write(" menit   : ");
            menit = Console.ReadLine();

            Console.Write("detik   : ");
            detik = Console.ReadLine();

            Console.Write("satuan waktu (AM/PM): ");
            waktu = Console.ReadLine();

            if (jam <= 12)
            {
                if (waktu == "PM")
                {
                    jam += 12;
                    jam = jam >= 24 ? jam - 24 : jam;
                    Console.WriteLine(jam.ToString().PadLeft(2, '0') + ":" + menit + ":" + detik);
                }
                else if (waktu == "AM")
                {
                    Console.WriteLine(jam.ToString().PadLeft(2, '0') + ":" + menit + ":" + detik);
                } 
            }
            else
            {
                Console.WriteLine("==============================");
                Console.WriteLine("Masukkan angka yang benar!");
            }
        }
        static void Soal8_Fibonacci()
        {
            int n1 = 1, n2 = 1, n3, i, nomor;
            Console.WriteLine("===== SOAL 8 FIBONACCI  =====");
            Console.Write("berapa banyak angka yang akan ditampilkan: ");
            nomor = int.Parse(Console.ReadLine());
            Console.Write(n1 + " " + n2 + " ");

            //int[] nomorArray = new int[number];

            //for (i=0; i < nomor; i++)
            //{ 
            // if (i <= 1)
            // {
            //nomorArray[i] = 1;
            //}
            // else
            //{
            //  nomorArray[i] = nomorArray[i-2] + nomorArray[i-1];
            //}
            //}
            // Console.Write(nomorArray[i] + " "); atau Console.Write(String.Join(" ", nomorArray[i]));


            for (i = 2; i < nomor; i++)
            {
                n3 = n1 + n2;

                Console.Write(n3 + " ");
                n1 = n2;
                n2 = n3;
            }

        }
        static void Soal7_deretAngka()
        {

            Console.WriteLine("===== SOAL 7 DERET ANGKA =====");
            int i, jmlDeret, angka;
            Console.Write("berapa banyak angka yang akan ditampilkan: ");
            jmlDeret = int.Parse(Console.ReadLine());

            Console.Write("masukkan angka :");
            angka = int.Parse(Console.ReadLine());
           

            for (i = 1; i <= jmlDeret; i++)
            {
                
                if (i % 2 == 0)
                {
                    Console.Write(i * angka + " ");
                }
                else
                {
                    Console.Write(i * angka * (-1) + " ");
                }
                
            }
        }
        static void Soal6_deretPangkat()
        {

            Console.WriteLine("===== SOAL 6  DERET PANGKAT =====");
            int i, jmlDeret = 1, angka = 1;
            string hasil;
            Console.Write("berapa banyak angka yang akan ditampilkan: ");
            jmlDeret = int.Parse(Console.ReadLine());

            for (i = 1; i <= jmlDeret; i++)
            {
                angka *= 3;
                hasil = angka.ToString();
                if (i % 2 == 0)
                {
                    Console.Write(" " + hasil.Replace(hasil, "*"));
                }
                else
                {
                    Console.Write(" " + hasil);
                }
            }
        }
        static void Soal5_HilangKata()
        {
            Console.WriteLine("===== SOAL 5  HILANG HURUF PERTAMA DALAM SATU KATA =====");
            Console.Write("masukkan kalimat : ");
            string[] kal = Console.ReadLine().Split(' ');
            // string tampung = "";


            for (int i = 0; i < kal.Length; i++)
            {
                //char [] chars = kal[i].ToArray();
                for (int j = 0; j < kal[i].Length; j++)
                {
                    // if (j > 0)
                    if (j == 0 && i == 0)
                    {
                     // tampung += chars[j] + " ";  
                        Console.Write(kal[i].Remove(0));
                        if( j == 0 && i == 0)
                        {
                            Console.Write(kal[i].Replace(kal[i], "***"));
                        }
                    }
                    
                    else
                    {
                        Console.Write(kal[i][j]);
                    }
                    //tampung += ' ';
                }
               
                Console.Write(' ');
            }
        }
        static void Soal4_gantiHurufTengah()
        {

            Console.WriteLine("===== SOAL 4 HILANG HURUF AWAL DAN AKHIR =====");
            Console.Write(" Masukkan kalimat :");
            string kalimat = Console.ReadLine();
            string[] kal = kalimat.Split(' ');

            for (int i = 0; i < kal.Length; i++)
            {
                for (int j = 0; j < kal[i].Length; j++)
                {
                    if (j == 0 )
                    {
                        Console.Write(kal[i].Replace(kal[i], "*"));
                    }
                    else if (j == kal[i].Length - 1)
                    {
                        Console.Write(kal[i].Replace(kal[i], "*"));
                    }
                    else
                    {
                        Console.Write(kal[i][j]);
                    }

                }
                Console.Write(' ');

            }

        }
        static void Soal2_Array()
        {
            Console.WriteLine("==== SOAL 2 ARRAY PISAH KATA====");
            Console.WriteLine(" Masukkan kalimat : ");
            string kalimat = Console.ReadLine();

            string[] kataArray = kalimat.Split(" ");

            foreach (string kata in kataArray)
            {
                Console.WriteLine(kata);
            }
            Console.WriteLine($"Total kata adalah 4");
        }
        static void Soal3_gantiHuruf()
        {

            Console.WriteLine("===== SOAL 3 HILANG HURUF TENGAH =====");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();
            string[] kal = kalimat.Split(' ');

            for (int i = 0; i < kal.Length; i++)
            {
                for (int j = 0; j < kal[i].Length; j++)
                {
                    if (j == 0)
                    {
                        Console.Write(kal[i][j]);
                    }
                    else if (j == kal[i].Length - 1)
                    {
                        Console.Write(kal[i][j]);
                    }
                    else
                    {
                        Console.Write(kal[i].Replace(kal[i], "*"));
                    }
                }

                Console.Write(' ');
            }
        }
        static void Soal1_GajiKaryawan()
        {

            double jamKerja, upah = 0, totalUpah = 0, upahKaryawan = 0, upahLembur = 0;
            int golongan;
            Console.WriteLine("===== SOAL 1 GAJI KARYAWAN =====");
            Console.Write("Masukkan Golongan  (1/2/3/4) :");
            golongan = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Jam Kerja           :");
            jamKerja = double.Parse(Console.ReadLine());

            switch (golongan)
            {
                case 1:
                    {
                        upah = 2000;
                        break;
                    }
                case 2:
                    {
                        upah = 3000;
                    }
                    break;
                case 3:
                    {
                        upah = 4000;
                    }
                    break;
                case 4:
                    {
                        upah = 5000;
                    }
                    break;
                default:
                    {
                        upah = 0;
                    }
                    break;

            }

            if (/*jamKerja == 0 &&*/ jamKerja <= 40)
            {
                upahKaryawan = jamKerja * upah;
                upahLembur = 0;
                totalUpah = upahKaryawan + upahLembur;

            }

            else if (jamKerja > 40)
            {
                upahKaryawan = 40 * upah;
                upahLembur = (jamKerja - 40) * 1.5 * upah;
                totalUpah = upahKaryawan + upahLembur;
            }
            else if(jamKerja <= 0)
            {
                Console.WriteLine("Masukkan angka yang benar!");
            }

            Console.WriteLine("================================");
            Console.WriteLine($"Upah    : {upahKaryawan}");
            Console.WriteLine($"lembur  : {upahLembur}");
            Console.WriteLine($"total   : {totalUpah}");
            Console.WriteLine("================================");

        }

    }
}   


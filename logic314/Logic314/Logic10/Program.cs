﻿using System;
using System.Collections.Generic;

namespace Logic10
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //SimpleArraySum();
            strongPass();
            
            Console.ReadKey();
        }
        static void strongPass()
        {
            Console.WriteLine("==== STRONG PASSWORD ====");

        }
       

        static void SimpleArraySum()
        {
            Console.WriteLine("==== SIMPLE ARRAY SUM ====");

            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(10);
            list.Add(11);

            int hasil = simpleArraySum(list);
            Console.WriteLine(hasil);
        }
        static int simpleArraySum(List<int> ar)
        {
            int hasil = 0;
            foreach (var angka in ar)
            {
                hasil += angka;
            }

                return hasil;
        }
    }
}

﻿using System;

namespace Logic04
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //removeString();
            //insertString();
            replaceString();

        }
        static void replaceString()
        {
            Console.WriteLine(" ==== REPLACE STRING ====");
            Console.Write("Masukkan kalimat :");
            string kalimat = Console.ReadLine();
            Console.Write("dari kata :");
            string katalama = (Console.ReadLine());
            Console.Write("replace menjadi kata :");
            string katabaru = Console.ReadLine();

            Console.WriteLine($"hasil replace string: {kalimat.Replace(katalama, katabaru)}");
        }
        static void insertString()
        {
            Console.WriteLine(" ==== INSERT STRING ====");
            Console.Write("Masukkan kalimat :");
            string kalimat = Console.ReadLine();
            Console.Write("isi parameter insert :");
            int parameter = int.Parse(Console.ReadLine());
            Console.Write("Masukkan input string :");
            string input = Console.ReadLine();

            Console.WriteLine($"hasil insert string: {kalimat.Insert(parameter, input)}");
        }
        static void removeString()
        {
            Console.WriteLine(" ==== Remove String ====");
            Console.Write("Masukkan kalimat :");
            string kalimat = Console.ReadLine();
            Console.Write("isi parameter remove :");
            int parameter = int.Parse(Console.ReadLine());

            Console.WriteLine($"hasil remove string : {kalimat.Remove(parameter)}");

        }
    }
}

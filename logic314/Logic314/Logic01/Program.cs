﻿using System;

namespace Logic01
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // konversi ();
            // operatorAritmatika();
            // modulus();
            //penugasan();
            //perbandingan();
            // operatorLogika();
            penjumlahan();


            Console.ReadKey();

        }

        static void penjumlahan()
        {
            int mangga, apel;

            Console.WriteLine("==== PENJUMLAHAN ====");
            Console.Write("jumlah mangga = ");
            mangga = Convert.ToInt32(Console.ReadLine());
            Console.Write("jumlah apel = ");
            apel = Convert.ToInt32(Console.ReadLine());

            int hasil = HitungJumlah(mangga, apel);

            Console.WriteLine($"Hasil mangga + apel = {hasil}");


        }

        static int HitungJumlah(int mangga, int apel)
        {
            int hasil = 0;
            hasil = mangga + apel;
            return hasil;
        }

        static void operatorLogika()
        {
            //Console.WriteLine("=====BOOLEAN====");
            Console.Write("masukkan umur = ");
            int umur = int.Parse(Console.ReadLine());
            Console.WriteLine("masukkan password = :");
            string password = Console.ReadLine();

            bool isAdult = umur > 18; //kondisi pertama
            bool isPasswordValid = password == "admin"; //kondisi kedua

            if (isAdult && isPasswordValid)
            {
                Console.WriteLine(" Sudah Dewasa dan Password Valid");
            }
            else if (isAdult && !isPasswordValid)
            {
                Console.WriteLine("Sudah dewasa dan password tidak valid");
            }
            else if (isAdult && isPasswordValid)
            {
                Console.WriteLine("Belum dewasa dan password Valid");
            }
            // else (isAdult && !isPasswordValid)
            //     { 
            //  Console.WriteLine("Belum dewasa dan password invalid");
            // }

        }


        static void perbandingan()
        {

            int angka1, angka2 = 0;


            Console.WriteLine("==== PERBANDINGAN ====");
            Console.WriteLine("Jumalah angka1 = ");
            angka1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Jumalah angka2 = ");
            angka2 = int.Parse(Console.ReadLine());

            Console.WriteLine("Hasil Perbandingan = ");
            Console.WriteLine($"angka1 > angka2  : {angka1 > angka2}");
            Console.WriteLine($"angka1 < angka2 : {angka1 < angka2}");
            Console.WriteLine($"angka1 => angka2 : {angka1 >= angka2}");
            Console.WriteLine($"angka1 =< angka2 : {angka1 <= angka2}");
            Console.WriteLine($"angka1 !> angka2 : {angka1 != angka2}");
            Console.WriteLine($"angka1 == angka2 : {angka1 == angka2}");
        }
        static void penugasan()
        {
            int angka1 = 10;
            int angka2 = 8;

            /*diisi ulang*/
            angka1 = 15;

            Console.WriteLine("===== PENUGASAN =====");
            Console.WriteLine($"angka1 = {angka1}");
            Console.WriteLine($"angka2 = {angka2}");

            angka2 += angka1;
            Console.WriteLine($"angka2 += {angka2}");

            angka1 = 15;
            angka2 = 8;
            angka2 -= angka1;
            Console.WriteLine($"angka2 -= {angka2}");

            angka1 = 15;
            angka2 = 8;
            angka2 *= angka1;
            Console.WriteLine($"angka2 *= {angka2}");

            angka1 = 15;
            angka2 = 8;
            angka2 %= angka1;
            Console.WriteLine($"angka2 %= {angka2}");

            angka1 = 15;
            angka2 = 8;
            angka2 /= angka1;
            Console.WriteLine($"angka2 /= {angka2}");

        }
        static void modulus()
        {
            int angka1, angka2, hasil = 0;

            Console.Write("====== MODULUS =====");
            Console.Write("Masukkan angka pertama = ");
            angka1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Masukkan angka kedua = ");
            angka2 = int.Parse(Console.ReadLine());

            hasil = angka1 % angka2;

            Console.WriteLine($"Hasil Modulus = {hasil}");
        }



        static void operatorAritmatika()
        {

            int mangga, apel, hasil = 0;

            Console.Write("====== PENAMMBAHAN =====");
            Console.Write("Jumlah Mangga = ");
            mangga = Convert.ToInt32(Console.ReadLine());
            Console.Write("Jumlah Apel = ");
            apel = int.Parse(Console.ReadLine());

            hasil = mangga + apel;

            Console.WriteLine($"Hasil Mangga + Apel = {hasil}");

        }


        static void konversi()
        {
            int umur = 19;
            String strUmur = umur.ToString();
            Console.WriteLine(strUmur);

            int myInt = 10;
            double myDouble = 5.25;
            bool myBool = false;

            string myString = Convert.ToString(myInt);
            double myConvDouble = Convert.ToDouble(myInt);
            int myConvInt = Convert.ToInt32(myDouble);
            string myConvString = Convert.ToString(myBool);

            Console.WriteLine("Convert int to String : " + myString);
            Console.WriteLine("Convert int to double : " + myConvDouble);
            Console.WriteLine("Convert double to int : " + myConvInt);
            Console.WriteLine("Convert bool to string : " + myConvString);

            Console.WriteLine(strUmur);

        }


    }
}
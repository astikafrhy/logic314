﻿using System;

namespace bilangan_prima
{
    internal class Program
    {
        static void Main(string[] args)
        {
            bilanganPrima();

            Console.ReadKey();
        }

        static void bilanganPrima()
        {
            bool prima = true;
            Console.Write("Masukkan bilangan prima : ");
            int bilangan = int.Parse(Console.ReadLine());


            if (bilangan >= 2)
            {
                for (int i = 2; i <= bilangan; i++)
                {
                    for (int j = 2; j < i; j++)
                    {
                        if((i % j) == 0)
                        {
                            prima = false;
                            break;
                        }
                    }
                    if (prima)
                    {
                        Console.WriteLine($"bilangan {i} adalah bilangan prima");
                        prima = true;
                    }
                }
            }


        }
    }
}

﻿

using System;

public class TUGAS
{
    public static void Main(string[] args)
    {
        //Soal1_NilaiMhs();
        // Soal2_Pulsa();
        //Soal3_Grabfood();
        //Soal4_Sopi();
        //Soal5_Generasi();
        //Soal6_GajiPokok();
        //Soal7_NilaiBMI();
        Soal8_NilaiRataRata();
    }
    static void Soal8_NilaiRataRata()
    {
        double nilaiRata, nilaiMTK, nilaiFisika, nilaiKimia;
        Console.WriteLine("==== SOAL 8 NILAI RATA RATA ====");
        Console.Write("Masukkan nilai MTK : ");
        nilaiMTK = double.Parse(Console.ReadLine());
        Console.Write("Masukkan nilai Fisika : ");
        nilaiFisika = double.Parse(Console.ReadLine());
        Console.Write("Masukkan nilai Kimia :");
        nilaiKimia = double.Parse(Console.ReadLine());

        nilaiRata = (nilaiMTK + nilaiFisika + nilaiKimia) / 3;

        if (nilaiRata >= 50)
        {
            Console.WriteLine($"Nilai Rata-Rata : {Math.Round(nilaiRata, 2)}");
            Console.WriteLine("Selamat");
            Console.WriteLine("Kamu Berhasil");
            Console.WriteLine("Kamu Hebat");
        }
        else if (nilaiRata < 50)
        {
            Console.WriteLine($"Nilai Rata-Rata : {Math.Round(nilaiRata, 2)}");
            Console.WriteLine("Maaf");
            Console.WriteLine("Kamu Gagal");
        }

    }
    static void Soal7_NilaiBMI()
    {
        double beratBadan, tinggiBadan, nilaiBMI, tB;
        string status;
        Console.WriteLine("==== SOAL 7 NILAI BMI ====");
        Console.Write("Masukkan berat badan anda (kg) :");
        beratBadan = double.Parse(Console.ReadLine());
        Console.Write("Masukkan tinggi badan anda(cm) :");
        tinggiBadan = double.Parse(Console.ReadLine());

        tB = tinggiBadan / 100;
        nilaiBMI = beratBadan / (tB * tB);

        if (nilaiBMI < 18.5)
        {
            status = "kurus";
        }
        else if (nilaiBMI > 18.5 && nilaiBMI < 25)
        {
            status = "Langsing/Sehat";
        }
        else if (nilaiBMI > 25)
        {
            status = "Gemuk";
        }
        else
        {
            status = "obesitas";
        }

        Console.WriteLine($"Nilai BMI anda adalah {nilaiBMI}");
        Console.WriteLine($"Anda termasuk berbadan {status}");
    }
    static void Soal6_GajiPokok()
    {
        string nama;
        int tunjangan, gapok, banyakBulan;
        double totalPajak, totalBpjs, totalGaji, totalGapok;
        double pajak;
        double bpjs = 0.03;
        Console.WriteLine("==== SOAL 6 GAJI POKOK ====");
        Console.Write(" Nama : ");
        nama = Console.ReadLine();
        Console.Write(" Tunjangan : ");
        tunjangan = int.Parse(Console.ReadLine());
        Console.Write(" Gapok : ");
        gapok = int.Parse(Console.ReadLine());
        Console.Write(" Banyak Bulan : ");
        banyakBulan = int.Parse(Console.ReadLine());

        // if (tunjangan == 0 && gapok == 0) || (tunjangan < 0 && gapok < 0)

        if (gapok + tunjangan <= 5000000)
        {
            pajak = 0.05;
        }
        else if ((gapok + tunjangan > 5000000) && (gapok + tunjangan < 10000000))
        {
            pajak = 0.1;
        }
        else if (gapok + tunjangan > 10000000)
        {
            pajak = 0.15;
        }
        else
        {
            pajak = 0;
        }

        totalPajak = (gapok + tunjangan) * pajak;
        totalBpjs = bpjs * (gapok + tunjangan);
        totalGapok = (gapok + tunjangan) - (totalPajak + totalBpjs);
        totalGaji = ((gapok + tunjangan) - (totalPajak + totalBpjs)) * banyakBulan;
        Console.WriteLine("=========================");
        Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut: ");
        Console.WriteLine($"Pajak : Rp.{totalPajak}");
        Console.WriteLine($"bpjs : Rp.{totalBpjs}");
        Console.WriteLine($"gaji/bln : Rp.{totalGapok}");
        Console.WriteLine($"Total Gaji/banyak bulan : Rp.{totalGaji}");
    }
    static void Soal5_Generasi()
    {
        string nama;
        int thnKelahiran;

        Console.WriteLine("==== SOAL 5 GENERASI ====");
        Console.Write("Masukkan nama anda: ");
        nama = Console.ReadLine();
        Console.Write("Tahun Berapa anda lahir ?: ");
        thnKelahiran = int.Parse(Console.ReadLine());


        if (thnKelahiran >= 1944 && thnKelahiran <= 1964)
        {
            Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Baby boomer");
        }
        else if (thnKelahiran >= 1965 && thnKelahiran <= 1979)
        {
            Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi X");
        }
        else if (thnKelahiran >= 1980 && thnKelahiran <= 1994)
        {
            Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Y (Milenials)");
        }
        else if (thnKelahiran >= 1995 && thnKelahiran <= 2015)
        {
            Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Z");
        }
        else
        {
            Console.WriteLine("tahun yang anda masukkan belum terdaftar!");
        }
    }
    static void Soal4_Sopi()
    {
        int belanja;
        int ongkosKrm, plhVoucher;
        int diskonOngkir, diskonBelanja, totalBelanja;

        Console.WriteLine("==== SOAL 4 SOPI ====");
        Console.Write("Belanja :");
        belanja = int.Parse(Console.ReadLine());
        Console.Write("Ongkos Kirim :");
        ongkosKrm = int.Parse(Console.ReadLine());
        Console.Write("Pilih Voucher :");
        plhVoucher = int.Parse(Console.ReadLine());

        switch (plhVoucher)
        {
            case 1:
                if (belanja >= 30000)
                {
                    diskonOngkir = 5000;
                    diskonBelanja = 5000;
                    totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
                }
                else
                {
                    diskonOngkir = 0;
                    diskonBelanja = 0;
                    totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
                }
                Console.WriteLine("=========================");
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Ongkos Kirim : {ongkosKrm}");
                Console.WriteLine($"Diskon Ongkir : {diskonOngkir}");
                Console.WriteLine($"Diskon Belanja : {diskonBelanja}");
                Console.WriteLine($"Total Belanja : {totalBelanja}");
                break;
            case 2:
                if (belanja >= 50000)
                {
                    diskonOngkir = 10000;
                    diskonBelanja = 10000;
                    totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
                }
                else
                {
                    diskonOngkir = 0;
                    diskonBelanja = 0;
                    totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
                }
                Console.WriteLine("=========================");
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Ongkos Kirim : {ongkosKrm}");
                Console.WriteLine($"Diskon Ongkir : {diskonOngkir}");
                Console.WriteLine($"Diskon Belanja : {diskonBelanja}");
                Console.WriteLine($"Total Belanja : {totalBelanja}");
                break;
            case 3:
                if (belanja >= 100000)
                {
                    diskonOngkir = 20000;
                    diskonBelanja = 10000;
                    totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
                }
                else
                {
                    diskonOngkir = 0;
                    diskonBelanja = 0;
                    totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
                }

                // BUAT DISKON ONGKIR YANG KURANG DARI 5000 BIAR GA KEPOTONG DARI DISKON ORDER
                //diskonOngkir = .... <= .... ? .... : .....

                Console.WriteLine("=========================");
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Ongkos Kirim : {ongkosKrm}");
                Console.WriteLine($"Diskon Ongkir : {diskonOngkir}");
                Console.WriteLine($"Diskon Belanja : {diskonBelanja}");
                Console.WriteLine($"Total Belanja : {totalBelanja}");
                break;
            default:
                Console.WriteLine("=========================");
                Console.WriteLine("voucher tidak tersedia!");
                break;


        }

    }
    static void Soal3_Grabfood()
    {
        int ongkir = 5000;
        int belanja, jarak;
        double totalBelanja, totalOngkir;
        string kodePromo;
        double diskon = 0.4;
        double totalDiskon = 0;

        Console.WriteLine("===== SOAL 3 GRAB FOOD ====");
        Console.Write("Belanja : ");
        belanja = int.Parse(Console.ReadLine());

        Console.Write("Jarak : ");
        jarak = int.Parse(Console.ReadLine());

        Console.Write("Masukan Kode Promo : ");
        kodePromo = Console.ReadLine().ToUpper();


        if (belanja >= 30000 && kodePromo == "JKTOVO")
        {
            if (jarak >= 5)
            {
                totalOngkir = ongkir + ((jarak - 5) * 1000);
            }
            else
            {
                totalOngkir = ongkir;
            }

            totalDiskon = belanja * diskon;
            totalBelanja = belanja - totalDiskon + totalOngkir;

            //Console.WriteLine("=========================");
            //Console.WriteLine($"Belanja : {belanja}");
            //Console.WriteLine($"Diskon 40%: {totalDiskon}");
            //Console.WriteLine($"Ongkir : {totalOngkir}");
            //Console.WriteLine($"Total Belanja : {totalBelanja}");

        }
        else
        {
            if (jarak <= 5)
            {
                totalOngkir = ongkir;

            }
            else
            {
                totalOngkir = ongkir + ((jarak - 5) * 1000);
            }

            totalBelanja = belanja + totalOngkir;

            //Console.WriteLine("=========================");
            //Console.WriteLine($"Belanja : {belanja}");
            //Console.WriteLine($"Diskon = - ");
            //Console.WriteLine($"Ongkir : {totalOngkir}");
            //Console.WriteLine($"Total Belanja : {totalBelanja}");
        }

        Console.WriteLine("=========================");
        Console.WriteLine($"Belanja : {belanja}");
        Console.WriteLine($"Diskon 40%: {totalDiskon}");
        Console.WriteLine($"Ongkir : {totalOngkir}");
        Console.WriteLine($"Total Belanja : {totalBelanja}");
    }
    static void Soal2_Pulsa()
    {
        // int point = 0;
        Console.WriteLine("===== SOAL 2 PULSA====");
        Console.Write("Pulsa : ");
        int pulsa = int.Parse(Console.ReadLine());

        if (pulsa >= 0 && pulsa <= 9999)
        {
            Console.WriteLine("Point : 0");
        }
        else if (pulsa >= 10000 && pulsa <= 24999)
        {
            Console.WriteLine(" Point : 80");
        }
        else if (pulsa >= 25000 && pulsa <= 49999)
        {
            Console.WriteLine(" Point : 200");
        }
        else if (pulsa >= 50000 && pulsa <= 99999)
        {
            Console.WriteLine("Point : 400");
        }
        else if (pulsa == 100000)
        {
            Console.WriteLine("Point : 800");
        }
        else if (pulsa < 0)
        {
            Console.WriteLine("gabisa ngutang");
        }
        // string ternanry = pulsa < 0 ? "Invalid" : pulsa.ToString();

        // Console.WriteLine($"Pulsa = {ternary));
        //Console.WriteLine($"Point : {point});
    }

    static void Soal1_NilaiMhs()
    {
        Console.WriteLine("===== SOAL 1 NILAI MAHASISWA ====");
        Console.Write("Masukkan nilai : ");
        int nilai = int.Parse(Console.ReadLine());

        if (nilai >= 90 && nilai <= 100)
        {
            Console.WriteLine("Grade A");
        }
        else if (nilai >= 70 && nilai <= 89)
        {
            Console.WriteLine(" Grade B");
        }
        else if (nilai >= 69 && nilai <= 50)
        {
            Console.WriteLine(" Grade C");
        }
        else if (nilai >= 0 && nilai <= 49)
        {
            Console.WriteLine(" Grade E");
        }
        else
        {
            Console.WriteLine("GAGAL TOTAL");
        }

    }
}

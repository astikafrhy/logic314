﻿using System;
using System.Collections.Generic;

namespace SimulasiLogic
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soal1();
            //soal2();
            //soal3();
            //soal4();
            //soal5();
            soal6();
            //soal7();
            //soal8();
            //soal9();
            //soal10();
            //soal11();
            //kisiSoal1();
           //kisiSoal2();

            Console.ReadKey();

        }

        static void kisiSoal2()
        {
            Console.WriteLine("==== ANGKA PRIMA ====");
            Console.Write("Masukkan bilangan : ");
            int bilangan = int.Parse(Console.ReadLine());

            bool prima = true;
            int tampung = 0;
            if(bilangan >= 2)
            {
                // untuk mencetak semua bilangan prima >> for(int i = 2; i <= bilangan; i++)
                //{
                for (int j = 2; j < bilangan; j++) // ganti "bilangan" jadi "i" untuk menampilkan semua bilangan prima
                {
                    if (bilangan % j == 0) // ganti "bilangan" jadi "i" untuk menampilkan semua bilangan prima
                    {
                        tampung = j;
                        prima = false;
                       // break; >> print pembagi terkecil
                    }
                }
                if (prima)
                {
                    Console.WriteLine($"1");
                }
                else
                {
                    Console.WriteLine(tampung); 
                }
                    prima = true;
                //}
            }
            else
            {
                Console.WriteLine("Inputan bukan bilangan Prima");
            }
        }
        static void kisiSoal1()
        {
            Console.WriteLine("=== HAPUS ANGKA GENAP ====");
            //  List<int> list = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            list.Add(6);
            list.Add(7);
            list.Add(8);
            list.Add(9);
            list.Add(10);

            for(int i = 0; i < list.Count; i++)
            {
                if(list[i] % 2 == 0)
                    list.RemoveAt(i);
            }

            Console.WriteLine(String.Join(", ", list));
        }
        static void soal11()
        {
            Console.WriteLine("==== SOAL 11 THE ONE NUMBER ====");
        }
        static void soal10()
        {
            Console.WriteLine("==== SOAL 10 ELEMEN ARRAY ====");
        }
        static void soal9()
        {
            Console.WriteLine("==== SOAL 9 BELI PULSA ====");
            Console.Write("Masukkan pulsa yang akan dibeli :");
            int pulsa = int.Parse(Console.ReadLine());
            int point= 0, point1 = 0, point2 = 0;
            int total = 0;

            if (pulsa >= 0 && pulsa <= 10000)
            {
                point =0;

                Console.WriteLine("========= =============================");
                Console.WriteLine($"0-10000     : {point} point ");
                Console.WriteLine($"Total Point = {point} point");
            }
            else if (pulsa >= 10001 && pulsa <= 30000)
            {
                point = 0;
                point1 = (pulsa - 10000) / 1000;
                total = point + point1;

                Console.WriteLine("======================================");
                Console.WriteLine($"0-10000     : {point} point ");
                Console.WriteLine($"10001-30000 : {point1} point");
                Console.WriteLine($"Total Point : {point} + {point1} = {total} point");
            }
            else if (pulsa >= 30000)
            {
                point = 0;
                point1 = 20;
                point2 = ((pulsa - 30000) / 1000) * 2;
                total = point + point1 + point2;

                Console.WriteLine("======================================");
                Console.WriteLine($"0-10000     : {point} point ");
                Console.WriteLine($"10001-30000 : {point1} point ");
                Console.WriteLine($">30000      : {point2} point");
                Console.WriteLine($"Total Point : {point} + {point1} + {point2} = {total} point");
            }
        }
        static void soal8()
        {
            Console.WriteLine("==== SOAL 8 REKRUSIF DIGIT ====");
        }
        static void soal7()
        {
            Console.WriteLine("==== SOAL 7 FIBONACI ====");
        }
        static void soal6()
        {
            Console.WriteLine("==== SOAL 6 PANGRAM ====");

            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine().ToLower();

            string alfabet = "abcdefghijklmnopqrstuvwxyz";
            
            bool pangram = true;
            foreach(char item in alfabet)
            {
                if (!kalimat.Contains(item))
                {
                    pangram = false;
                    break;
                }
            }
            if (pangram)
            {
                Console.WriteLine("Kalimat ini adalah pangram");
            }
            else
            {
                Console.WriteLine("Kalimat ini bukan pangram");
            }
        }
        static void soal5()
        {
            Console.WriteLine("==== SOAL 5 NILAI ====");
        }
        static void soal4()
        {
            Console.WriteLine("==== SOAL 4 BANJIR ====");
        }
        static void soal3()
        {
            Console.WriteLine("==== SOAL 3 KERANJANG BUAH ====");
            
        }
        static void soal2()
        {
            Console.WriteLine("====SOAL 2 INVOICE PENJUALAN ====");
            Console.Write("Start : ");
            int start = int.Parse(Console.ReadLine());
            Console.Write("End   : ");
            int end = int.Parse(Console.ReadLine());

            string inisial = "XA-";
            DateTime date = DateTime.Now;

            for (int i = start; i <= end; i++)
            {
                Console.WriteLine($"{inisial}{date.ToString("ddMMyyyy")}-{i.ToString().PadLeft(5,'0')}");
            }



        }
        static void soal1()
        {
            Console.WriteLine("==== SOAL 1 HITUNG KATA ====");
            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();
            int hasil = 0;
            int index = 0;

            for (int i = 0; i < kalimat.Length; i++)
            {
                if (char.IsUpper(kalimat[i]) || index == 0)
                {
                    hasil++;
                }
                index++;
            }
            Console.WriteLine($"Banyak Kata : {hasil}");
        }
    }
} 

﻿using System;

namespace Logic03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Tugas2();
            //Tugas3();
            //Tugas4();
            //Tugas5();



            //Soal1_nilaiMahasiswa(); 
            //Soal2_belipulsa();      
            //Soal3_Grabfood();
            //Soal4_Sopi();
            //Soal5_Generasi();
            //Soal6_GajiPokok();
            //Soal7_NilaiBMI();
            //Soal8_NilaiRataRata();



            //PerulanganWhile() ;
            //DoWhile();
            //PerulanganForIncreement();
            //PerulanganForDecreement();
            //Break();
            //breakCouns();
            //forBersarang();
            //ArrayForeach();
            //Array();
            //arrayfor();
            array2dimensi();
            //array2dimensiFor();
            //splitJoin();
            //substring();
            // stringTocharArray();


            Console.ReadKey();
        }
        static void stringTocharArray()
        {
            Console.WriteLine("==== String To Char Array ====");
            Console.Write(" Masukkan kalimat : ");
            string kalimat = Console.ReadLine();

            char[] array = kalimat.ToCharArray();
            foreach (char c in array)
            {
                Console.WriteLine(c);
            }

        }
        static void substring()
        {
            Console.WriteLine("==== SUBSTRING ====");
            Console.Write("masukkan kalimat : ");
            string kalimat = Console.ReadLine();

            Console.WriteLine(" substring (1,4) : " + kalimat.Substring(1, 4));
            Console.WriteLine(" substring (5,2) : " + kalimat.Substring(5, 2));
            Console.WriteLine(" substring (7,9) : " + kalimat.Substring(7, 9));
            Console.WriteLine("substring (9) : " + kalimat.Substring(9));

        }
        static void splitJoin()
        {
            Console.WriteLine("==== SPLIT JOIN ====");
            Console.WriteLine(" Masukkan kalimat : ");
            string kalimat = Console.ReadLine();
            string[] kataArray = kalimat.Split(" ");

            foreach (string kata in kataArray)
            {
                Console.WriteLine(kata);
            }
            Console.WriteLine(string.Join(",", kataArray));
        }
        static void array2dimensiFor()
        {
            int[,] array = new int[,]
          {
                {1, 2, 3} ,
                {4, 5, 6} ,
                {7, 8, 9}
          };
            //cetak menggunkan for 
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write(array[i, j] + " ");
                }
                Console.WriteLine();
            }
        }
        static void array2dimensi()
        {
            Console.WriteLine("==== ARRAY 2 DIMENSI =====");
            int[,] array = new int[,]
            {
                {1, 2, 3} ,
                {4, 5, 6} ,
                {7, 8, 9}
            };
            Console.WriteLine(array[0, 0] + array[1,1] + array[2,2]);
        }
        static void arrayfor()
        {
            Console.WriteLine("==== ARRAY FOR ====");
            string[] array = new string[]
            {
                "ASTIKA FUJIANTI RAHAYU","ISNI MORKONI ","YAYA" ,"RAISA","DANU"
            };

            //baca array menggunakan foreach
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
        }
        static void Array()
        {
            Console.WriteLine("==== ARRAY =====");
            int[] staticIntArray = new int[3];

            //isi array
            staticIntArray[0] = 1;
            staticIntArray[1] = 2;
            staticIntArray[2] = 3;


            //cetak array
            Console.WriteLine(staticIntArray[0]);
            Console.WriteLine(staticIntArray[1]);
            Console.WriteLine(staticIntArray[2]);

        }
        static void ArrayForeach()
        {
            Console.WriteLine("==== FOREACH ====");
            string[] array = new string[]
            {
                "ASTIKA FUJIANTI RAHAYU","ISNI MORKONI ","YAYA" ,"RAISA","DANU"
            };

            //baca array menggunakan foreach

            foreach (var item in array)
            {
                Console.WriteLine(item);
            }

        }
        static void forBersarang()
        {
            Console.WriteLine("==== FOR BERSARANG ====");
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write($"[{i},{j}]");
                }
                Console.Write("\n");
            }

        }
        static void Soal8_NilaiRataRata()
        {
            double nilaiRata, nilaiMTK, nilaiFisika, nilaiKimia;
            Console.WriteLine("==== SOAL 8 NILAI RATA RATA ====");
            Console.Write("Masukkan nilai MTK : ");
            nilaiMTK = double.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai Fisika : ");
            nilaiFisika = double.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai Kimia :");
            nilaiKimia = double.Parse(Console.ReadLine());

            nilaiRata = (nilaiMTK + nilaiFisika + nilaiKimia) / 3;

            if (nilaiRata >= 50)
            {
                Console.WriteLine($"Nilai Rata-Rata : {nilaiRata}");
                Console.WriteLine("Selamat");
                Console.WriteLine("Kamu Berhasil");
                Console.WriteLine("Kamu Hebat");
            }
            else if (nilaiRata < 50)
            {
                Console.WriteLine($"Nilai Rata-Rata : {nilaiRata}");
                Console.WriteLine("Maaf");
                Console.WriteLine("Kamu Gagal");
            }

        }
        static void Soal7_NilaiBMI()
        {
            int beratBadan;
            double tinggiBadan, nilaiBMI, tB;
            string status = Console.ReadLine();
            Console.WriteLine("==== SOAL 7 NILAI BMI ====");
            Console.Write("Masukkan berat badan anda (kg) :");
            beratBadan = int.Parse(Console.ReadLine());
            Console.Write("Masukkan tinggi badan anda(cm) :");
            tinggiBadan = int.Parse(Console.ReadLine());

            tB = tinggiBadan / 100;
            nilaiBMI = beratBadan / (tB * tB);

            if (nilaiBMI < 18.5)
            {
                status = "kurus";
            }
            else if (nilaiBMI > 18.5 && nilaiBMI < 25)
            {
                status = "Langsing/Sehat";
            }
            else if (nilaiBMI > 25)
            {
                status = "Gemuk";
            }

            Console.WriteLine($"Nilai BMI anda adalah {nilaiBMI}");
            Console.WriteLine($"Anda termasuk berbadan {status}");
        }
        static void Soal6_GajiPokok()
        {
            string nama;
            int tunjangan, gapok, banyakBulan;
            double totalPajak, totalBpjs, totalGaji, totalGapok;
            double pajak;
            double bpjs = 0.03;
            Console.WriteLine("==== SOAL 5 GAJI POKOK ====");
            Console.Write(" Nama : ");
            nama = Console.ReadLine();
            Console.Write(" Tunjangan : ");
            tunjangan = int.Parse(Console.ReadLine());
            Console.Write(" Gapok : ");
            gapok = int.Parse(Console.ReadLine());
            Console.Write(" Banyak Bulan : ");
            banyakBulan = int.Parse(Console.ReadLine());

            if (gapok + tunjangan <= 5000000)
            {
                pajak = 0.05;
            }
            else if ((gapok + tunjangan > 5000000) && (gapok + tunjangan < 10000000))
            {
                pajak = 0.1;
            }
            else if (gapok + tunjangan > 10000000)
            {
                pajak = 0.15;
            }
            else
            {
                pajak = 0;
            }

            totalPajak = (gapok + tunjangan) * pajak;
            totalBpjs = bpjs * (gapok + tunjangan);
            totalGapok = (gapok + tunjangan) - (totalPajak + totalBpjs);
            totalGaji = ((gapok + tunjangan) - (totalPajak + totalBpjs)) * banyakBulan;
            Console.WriteLine("=========================");
            Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut: ");
            Console.WriteLine($"Pajak : Rp.{totalPajak}");
            Console.WriteLine($"bpjs : Rp.{totalBpjs}");
            Console.WriteLine($"gaji/bln : Rp.{totalGapok}");
            Console.WriteLine($"Total Gaji/banyak bulan : Rp.{totalGaji}");
        }
        static void Soal5_Generasi()
        {
            string nama;
            int thnKelahiran;

            Console.WriteLine("==== SOAL 5 GENERASI ====");
            Console.Write("Masukkan nama anda: ");
            nama = Console.ReadLine();
            Console.Write("Tahun Berapa anda lahir ?: ");
            thnKelahiran = int.Parse(Console.ReadLine());


            if (thnKelahiran >= 1944 && thnKelahiran <= 1964)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Baby boomer");
            }
            else if (thnKelahiran >= 1965 && thnKelahiran <= 1979)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi X");
            }
            else if (thnKelahiran >= 1980 && thnKelahiran <= 1994)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Y (Milenials)");
            }
            else if (thnKelahiran >= 1995 && thnKelahiran <= 2015)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Z");
            }
            else
            {
                Console.WriteLine("tahun yang anda masukkan belum terdaftar!");
            }
        }
        static void Soal4_Sopi()
        {
            int belanja;
            int ongkosKrm, plhVoucher;
            int diskonOngkir, diskonBelanja, totalBelanja;

            Console.WriteLine("==== SOAL 4 SOPI ====");
            Console.Write("Belanja :");
            belanja = int.Parse(Console.ReadLine());
            Console.Write("Ongkos Kirim :");
            ongkosKrm = int.Parse(Console.ReadLine());
            Console.Write("Pilih Voucher :");
            plhVoucher = int.Parse(Console.ReadLine());

            switch (plhVoucher)
            {
                case 1:
                    if (belanja >= 30000)
                    {
                        diskonOngkir = 5000;
                        diskonBelanja = 5000;
                        totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
                    }
                    else
                    {
                        diskonOngkir = 0;
                        diskonBelanja = 0;
                        totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
                    }
                    Console.WriteLine("=========================");
                    Console.WriteLine($"Belanja : {belanja}");
                    Console.WriteLine($"Ongkos Kirim : {ongkosKrm}");
                    Console.WriteLine($"Diskon Ongkir : {diskonOngkir}");
                    Console.WriteLine($"Diskon Belanja : {diskonBelanja}");
                    Console.WriteLine($"Total Belanja : {totalBelanja}");
                    break;
                case 2:
                    if (belanja >= 50000)
                    {
                        diskonOngkir = 10000;
                        diskonBelanja = 10000;
                        totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
                    }
                    else
                    {
                        diskonOngkir = 0;
                        diskonBelanja = 0;
                        totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
                    }
                    Console.WriteLine("=========================");
                    Console.WriteLine($"Belanja : {belanja}");
                    Console.WriteLine($"Ongkos Kirim : {ongkosKrm}");
                    Console.WriteLine($"Diskon Ongkir : {diskonOngkir}");
                    Console.WriteLine($"Diskon Belanja : {diskonBelanja}");
                    Console.WriteLine($"Total Belanja : {totalBelanja}");
                    break;
                case 3:
                    if (belanja >= 100000)
                    {
                        diskonOngkir = 20000;
                        diskonBelanja = 20000;
                        totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
                    }
                    else
                    {
                        diskonOngkir = 0;
                        diskonBelanja = 0;
                        totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
                    }
                    Console.WriteLine("=========================");
                    Console.WriteLine($"Belanja : {belanja}");
                    Console.WriteLine($"Ongkos Kirim : {ongkosKrm}");
                    Console.WriteLine($"Diskon Ongkir : {diskonOngkir}");
                    Console.WriteLine($"Diskon Belanja : {diskonBelanja}");
                    Console.WriteLine($"Total Belanja : {totalBelanja}");
                    break;
                default:
                    Console.WriteLine("=========================");
                    Console.WriteLine("voucher tidak tersedia!");
                    break;
            }

        }
        static void Soal3_Grabfood()
        {
            int ongkir = 5000;
            int belanja, jarak;
            double totalBelanja, totalOngkir;
            string kodePromo;
            double diskon = 0.4;
            double totalDiskon;

            Console.WriteLine("===== SOAL 3 GRAB FOOD ====");
            Console.Write("Belanja : ");
            belanja = int.Parse(Console.ReadLine());

            Console.Write("Jarak : ");
            jarak = int.Parse(Console.ReadLine());

            Console.Write("Masukan Kode Promo : ");
            kodePromo = Console.ReadLine().ToUpper();


            if (belanja >= 30000 && kodePromo == "JKTOVO")
            {
                if (jarak >= 5)
                {
                    totalOngkir = ongkir + ((jarak - 5) * 1000);
                }
                else
                {
                    totalOngkir = ongkir;
                }

                totalDiskon = belanja * diskon;
                totalBelanja = belanja - totalDiskon + totalOngkir;

                Console.WriteLine("=========================");
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Diskon 40%: {totalDiskon}");
                Console.WriteLine($"Ongkir : {totalOngkir}");
                Console.WriteLine($"Total Belanja : {totalBelanja}");

            }
            else
            {
                if (jarak <= 5)
                {
                    totalOngkir = ongkir + ((jarak - 5) * 1000);

                }
                else
                {
                    totalOngkir = ongkir;
                }

                totalBelanja = belanja + totalOngkir;

                Console.WriteLine("=========================");
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Diskon = - ");
                Console.WriteLine($"Ongkir : {totalOngkir}");
                Console.WriteLine($"Total Belanja : {totalBelanja}");
            }
        

    }
    static void Soal2_belipulsa()
    {
        int pulsa;
        Console.WriteLine("===== PULSA =====");
        Console.Write("Pulsa : ");
        pulsa = int.Parse(Console.ReadLine());


        if (pulsa >= 1 && pulsa <= 24999)
        {
            Console.WriteLine("Point : 80 ");
        }
        else if (pulsa >= 25000 && pulsa <= 49999)
        {
            Console.WriteLine("Point : 200");
        }
        else if (pulsa >= 50000 && pulsa <= 99999)
        {
            Console.WriteLine("Point : 400 ");
        }
        else if (pulsa == 100000)
        {
            Console.WriteLine("Point : 800 ");
        }



    }
    static void Soal1_nilaiMahasiswa()
    {
        Console.WriteLine("==== NILAI MAHASISWA ====");
        Console.WriteLine("Masukkan nilai : ");
        int nilaiMhs = int.Parse(Console.ReadLine());

        if (nilaiMhs >= 90 && nilaiMhs <= 100)
        {
            Console.WriteLine("Grade A");
        }
        else if (nilaiMhs >= 70 && nilaiMhs <= 89)
        {
            Console.WriteLine("Grade B");
        }
        else if (nilaiMhs >= 50 && nilaiMhs <= 69)
        {
            Console.WriteLine("Grade C");
        }
        else if (nilaiMhs >= 1 && nilaiMhs <= 49)
        {
            Console.WriteLine("Grade E");
        }
        else
        {
            Console.WriteLine("GAGALLLLLLLLLLLLLL, COBA LAGI YUKK!!");
            Console.ReadKey();
        }
    }
    static void breakCouns()
    {
        for (int i = 0; i < 10; i++)
        {
            if (i == 5)
            {
                continue;
            }
            Console.WriteLine(i);
        }
    }

    static void Break()
    {
        for (int i = 0; i < 10; i++)
        {
            if (i == 5)
            {
                break;
            }
            Console.WriteLine(i);
        }
    }
    static void PerulanganForDecreement()
    {
        for (int i = 10; i > 0; i--)
        {
            Console.WriteLine(i);
        }
    }
    static void PerulanganForIncreement()
    {
        Console.WriteLine("==== PERULANGAN FOR =====");
        for (int i = 0; i < 10; i++)
        {
            Console.WriteLine(i);
        }
    }
    static void DoWhile()
    {
        Console.WriteLine("===== PERULANGAN DO WILE =====");
        int a = 0;
        do
        {
            Console.WriteLine(a);
            a++;
        }
        while (a < 10);
    }

    static void PerulanganWhile()
    {
        Console.WriteLine("==== PERULANGAN WHILE ====");
        bool ulangi = true;
        int nilai = 1;

        while (ulangi)
        {
            Console.WriteLine($" proses ke : {nilai}");
            nilai++;

            Console.WriteLine($"Apakah anda akan mengulangi proses ? (y/n) ");
            string input = Console.ReadLine().ToLower();
            if (input == "y")
            {
                ulangi = true;
            }
            else if (input == "n")
            {
                ulangi = false;
            }
            else
            {
                Console.WriteLine("input yang anda masukan salah");
                ulangi = true;
            }

        }


    }

    static void Tugas5()
    {
        int nilai;
        Console.WriteLine("===== TUGAS 5 GANJIL GENAP =====");
        Console.Write("Masukkan nilai : ");
        nilai = Convert.ToInt32(Console.ReadLine());

        if (nilai % 2 == 0)
        {
            Console.WriteLine("Bilangan Genap");
        }
        else
        {
            Console.WriteLine("Bilangan ganjil");
        }

    }

    static void Tugas4()
    {
        Console.WriteLine("===== TUGAS 4 PENILAIAN ====");
        Console.Write("Masukkan nilai : ");
        int nilai = int.Parse(Console.ReadLine());
        int nilaiMax = 100;

        if (nilai >= 80 && nilai <= nilaiMax)
        {
            Console.WriteLine("A");
        }
        else if (nilai >= 60 && nilai < 80)
        {
            Console.WriteLine("B");
        }
        else if (nilai >= 1 && nilai < 60)
        {
            Console.WriteLine("C");
        }

        else
        {
            Console.WriteLine(" gagal total");
            Console.ReadKey();
        }

    }

    static void Tugas3()
    {
        int batang, rokok, keuntungan, sisa;
        Console.WriteLine("===== TUGAS 3 ROKOK =====");
        Console.Write("Berapa banyak puntung yang di kumpulkan ? ");
        rokok = Convert.ToInt32(Console.ReadLine());

        batang = rokok / 8;
        sisa = rokok % 8;

        Console.WriteLine($"Banyaknya batang yang dikumpulkan = {0}", batang);
        Console.WriteLine($"Sisa batang adalah =  {0}\n", sisa);

        keuntungan = batang * 500;
        Console.WriteLine($"Penghasilan yang dihasilkan adalah = {0} , 00\n", keuntungan);

        Console.WriteLine("selesaii....");
        Console.ReadKey();

    }
    static void Tugas2()
    {
        int angka, pembagi;
        int hasilModulus = int.Parse(Console.ReadLine());
        Console.WriteLine("==== TUGAS 2 MODULUS ====");
        Console.WriteLine("Masukkan angka : ");
        angka = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Masukkan pembagi :");
        pembagi = Convert.ToInt32(Console.ReadLine());

        if (angka % pembagi == 0)
        {
            Console.WriteLine(" hasil modulus sama dengan 0");
        }

        if (angka % pembagi != 0)
        {
            Console.WriteLine("Hasil modulus bukan sama dengan 0");
            Console.ReadKey();
        }

    }

}
}

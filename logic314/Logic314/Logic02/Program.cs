﻿using System;

namespace Logic02
{
    internal class Program
    {
        static void Main(string[] args)
        {

            // ifstatement();

            //elseStatement();
            //ifNested();
            //ternary();
            //switchCase();
            //Tugas1();
            // Tugas1_Soal2();




            Console.ReadKey();

        }

        static void Tugas1()
        {
            Console.WriteLine("===== TUGAS 1 SOAL NO 1=====");
            double r;
            const double phi = 3.14;
            Console.Write("Masukkan nilai R : ");
            r = double.Parse(Console.ReadLine());

            double kelilingLingkaran = keliling(r);
            Console.WriteLine($"Hasil Keliling Lingkaran = {kelilingLingkaran}");

            double luasLingkaran = luas(r);
            Console.WriteLine($"Hasil Luas Lingkaran = {luasLingkaran}");

            static double keliling(double r)
            {
                double keliling = 0;
                keliling = 2 * 3.14 * r;
                return keliling;
            }

            static double luas(double r)
            {
                double luas = 0;
                luas = 3.14 * r * r;
                return luas;
            }


        }

        static void Tugas1_Soal2()
        {
            Console.WriteLine("===== NO 2 =====");
            double s;
            Console.Write("Masukkan nilai S : ");
            s = double.Parse(Console.ReadLine());

            double kelilingPersegi = keliling(s);
            Console.WriteLine($"Hasil Keliling Persegi = {kelilingPersegi}");

            double luasPersegi = luas(s);
            Console.WriteLine($"Hasil Luas Persegi = {luasPersegi}");

            static double keliling(double s)
            {
                double keliling = 0;
                keliling = 4 * s;
                return keliling;
            }

            static double luas(double s)
            {
                double luas = 0;
                luas = s * s;
                return luas;
            }






            static void switchCase()
            {
                Console.WriteLine("==== SWITCH CASE ====");
                Console.WriteLine("Silahkan pilih buah yang anda suka? (apel/mangga/jeruk)");
                string input = Console.ReadLine().ToLower();


                switch (input)
                {
                    case "apel":
                        Console.WriteLine("Anda memilih buah apel");
                        break;
                    case "mangga":
                        Console.WriteLine("Anda Memilih buah mangga");
                        break;
                    case "jeruk":
                        Console.WriteLine("Anda memilih buah jeruk");
                        break;
                    default:
                        Console.WriteLine("buah sedang kosong");
                        break;
                }
            }

            static void ternary()
            {
                Console.WriteLine("===== TERNARY =====");
                int x, y;
                Console.Write("Masukkan nilai x : ");
                x = int.Parse(Console.ReadLine());
                Console.Write("Masukkan nilai y : ");
                y = int.Parse(Console.ReadLine());

                string HasilTernary = x > y ? " Nilai x lebih besar dari nilai y" :
                  x < y ? "Nilai x lebih kecil dari nilai y" : "Nilai x sama dengan nilai y";

                Console.WriteLine(HasilTernary);


            }

            static void ifNested()
            {
                Console.WriteLine("===== IF NESTED ====");
                Console.Write("Masukkan nilai : ");
                int nilai = int.Parse(Console.ReadLine());
                int maxNilai = 100;

                if (nilai >= 70 && nilai <= maxNilai)

                {
                    Console.WriteLine("you got it !!");
                    if (nilai == 100)
                    {
                        Console.WriteLine("YUHUUU PERFECT!!!");
                    }
                    else if (nilai >= 0 && nilai > 70)
                    {
                        Console.WriteLine("KAMU GAGAL, COBA LAGI YGY!");

                    }

                    else
                    {
                        Console.WriteLine("coba lagi ygy");
                    }
                }
                static void elseStatement()
                {
                    Console.WriteLine("==== ELSE STATEMENT ====");
                    int x, y;
                    Console.Write("Masukkan nilai x :");
                    x = int.Parse(Console.ReadLine());
                    Console.Write("Masukkan nilai y : ");
                    y = int.Parse(Console.ReadLine());

                    if (x > y)
                    {
                        Console.WriteLine("Nilai x lebih besar dari nilai y");
                    }
                    else if (y > x)
                    {
                        Console.WriteLine("nilai y lebih besar dari nilai x");
                    }
                    else
                    {
                        Console.WriteLine(" nilai x sama dengan nilai y");
                    }


                }

                static void ifstatement()
                {
                    Console.WriteLine("==== IF STATEMENT ====");
                    int x, y;
                    Console.Write("Masukkan nilai x : ");
                    x = int.Parse(Console.ReadLine());
                    Console.Write("Masukkan nilai y : ");
                    y = int.Parse(Console.ReadLine());

                    if (x > y)
                    {
                        Console.WriteLine("Nilai x lebih besar dari nilai y");
                    }
                    if (y > x)
                    {
                        Console.WriteLine("niali y lebih besar dari nilai x");
                    }
                    if (x == y)
                    {
                        Console.WriteLine("nilai x sama dengan nilai y");
                    }
                }
            }
        }
    }
}


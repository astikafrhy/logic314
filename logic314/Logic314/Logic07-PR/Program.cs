﻿using System;
using System.Linq;

namespace Logic07_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soal1(); 
            //soal2_SOS();
            //soal3(); 
            //soal4(); 
            //soal5(); 
            //soal6();
            //soal6_dua();
            //soal7();
            //soal8(); 
            //soal9(); 
            //soal10();
            //soal11();
            //soal12();
            
            Console.ReadKey();
        }
            static void soal6_dua()
            {
            
            Console.WriteLine("==== SOAL 6 huruf bintang bintang ====");
            Console.Write("Masukkan kalimat : ");
            string kal = Console.ReadLine().ToLower();
            string trim = String.Concat(kal.Where(a => !Char.IsWhiteSpace(a)));

            foreach (char c in trim) { 
                Console.Write("***" + c + "***");
                Console.WriteLine();
                }
            }
            static void soal9()
            {
                Console.WriteLine("==== SOAL 9 ==== ");
                int[,] array = new int[,]
                {
                    {11,  2,   4} ,
                    {4 ,  5,   6} ,
                    {10,  8, -12}
                };

                int hDiagonal1 = array[0, 0] + array[1, 1] + array[2, 2];
                Console.WriteLine("Hasil dari Diagonal pertama : " + array[0,0] + " + " + array[1, 1] + " - "  + Math.Abs(array[2, 2]) + " = " +hDiagonal1);
                //Console.WriteLine($"hasil dari diagonal pertama = {hDiagonal1}");

                int hDiagonal2 = array[2, 0] + array[1, 1] + array[0, 2];
                Console.WriteLine("Hasil dari Diagonal pertama : " + array[2, 0] + " + " + array[1, 1] + " + " + array[0, 2] + " = " + hDiagonal2);
                //Console.WriteLine($"hasil dari diagonal kedua = {hDiagonal2}");

                int selisih = hDiagonal1 - hDiagonal2;
                Console.WriteLine("Perbedaan Diagonal          : " + hDiagonal1 + " - " + hDiagonal2 + " = " + selisih);
            }
            static void soal10()
            {
                Console.WriteLine("==== SOAL 10 ====");
                Console.Write("Masukkan panjang lilin :");
                int[] lilin = Array.ConvertAll(Console.ReadLine().Split(" "),int.Parse);

                int maks = 0, banyakMaks = 0;
                for(int j = 0; j < lilin.Length; j++)
                {
                    if(lilin[j] > maks)
                        maks = lilin[j];
                    if (lilin[j] == maks)
                        banyakMaks++;
            }
                Console.WriteLine("Nilai lilin tertinggi adalah : " + maks);
                Console.WriteLine("Jumlah lilin tertinggi sebanyak : " + banyakMaks);
            }
            static void soal11()
            {
                Console.WriteLine("==== SOAL 11 ====");
                Console.WriteLine("Masukkan angka : ");
                int [] angka = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
                Console.Write("Jumlah Posisi : ");
                int rotasi = int.Parse(Console.ReadLine());
                int rot = 0;

                for(int i = 0; i < rotasi; i++)
                {
                    for (int j = 0; j < angka.Length; j++)
                {
                    if (j < 1)
                    {
                        rot = angka[j];
                        angka[j] = angka[j + 1];
                    }
                    else if (j >= 1 && j < angka.Length - 1)
                        angka[j] = angka[j + 1];
                    else
                        angka[angka.Length - 1] = rot;
                    }
                }
             Console.Write(String.Join(" ", angka));   
            }
            static void soal12()
            {
                Console.WriteLine("==== SOAL 12 SORTING ====");
                Console.Write("Masukan banyak data =");

                int jumlah = int.Parse(Console.ReadLine());
                int[] array = new int[jumlah];

                for (int Jnilai = 0; Jnilai < jumlah; Jnilai++)
                {
                    Console.Write("Masukan Elemen ke " + (Jnilai + 1) + " =");
                    array[Jnilai] = int.Parse(Console.ReadLine());
                    }
                    Console.Write("Data di urutkan menggunakan Bubble Sort = ");
                    BubbleSort1(array);
                    Console.ReadKey();

                    static void BubbleSort1(int[] Jnilai)
                    {
                        for (int i = Jnilai.Length - 1; i > 0; i--)
                        {
                            for (int j = 0; j < i; j++)
                            {
                                if (Jnilai[j] > Jnilai[j + 1])
                                {
                                    int temp = Jnilai[j];
                                    Jnilai[j] = Jnilai[j + 1];
                                    Jnilai[j + 1] = temp;
                                }
                            }
                        }
                        for (int ii = 0; ii < Jnilai.Length; ii++)
                            Console.Write(Jnilai[ii] + " ");
                    }

            }
            static void soal7()
            {
                int sisa = 0;
                int totalHargaMenu = 0;
                int bisaDiMakan = 0;
                int totalBayar = 0;
                int uang = 0;
                Console.WriteLine("==== SOAL 7  MAKAN ====");

                Console.Write("Masukkan total menu : ");
                int totalMenu = int.Parse(Console.ReadLine());
                Console.Write("Masukkan index makanan alergi : ");
                int index = int.Parse(Console.ReadLine());

                    ulang:
                Console.Write("Masukkan harga menu : ");
                int[] hargaMenu = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

                if (hargaMenu.Length > totalMenu)
                {
                    Console.WriteLine("Index inputan berlebih, ulangi lagi!");
                    goto ulang;
                }
                else
                {
                    Console.Write("masukkan uang anda : ");
                    uang = int.Parse(Console.ReadLine());

                    for (int i = 0; i < hargaMenu.Length; i++)
                    {
                        totalHargaMenu += hargaMenu[i];
                    }

                    bisaDiMakan = totalHargaMenu - hargaMenu[index];
                    totalBayar = bisaDiMakan / 2;
                    sisa = uang - totalBayar;

                    if (sisa < 0)
                    {
                        Console.WriteLine($"Anda harus membayar = " + totalBayar.ToString("Rp #,##"));
                        Console.WriteLine($"Sisa Uang Anda = " + sisa.ToString("Rp. #,##"));
                    }
                    else if (sisa == 0)
                    {
                        Console.WriteLine("Uang Anda pas tidak ada kembalian");
                    }
                    else
                    {
                        Console.WriteLine($"Uang anda Kurang " + sisa.ToString("Rp. #,##"));
                    }
                }
                }
            static void soal8_dua()
            {
                int angka, i, j;
                Console.WriteLine("==== SOAL 8 BINTANG SEGITIGA SIKU SIKU ====");
                Console.Write("Masukkan kolom bintang : ");
                angka = Convert.ToInt32(Console.ReadLine());

                for (i = 0; i < angka; i++)
                {
                    for (j = 0; j < angka; j++)
                    {
                        if (j < angka - 1 - i)
                        {
                            Console.Write(" ");
                        }
                        else
                        {
                            Console.Write("*");
                        }
                    }
                }
            }
            static void soal8()
            {
                int angka, i, j;
                Console.WriteLine("==== SOAL 8 BINTANG SEGITIGA SIKU SIKU ====");
                Console.Write("Masukkan kolom bintang : ");
                angka = Convert.ToInt32(Console.ReadLine());

                for (i = 0; i <= angka; i++)
                {
                    int s = angka - i;
                    for (j = 1; j <= s; j++)
                    {
                        Console.Write(" ");
                    }
                    for (j = 1; j <= i; j++)
                    {
                        Console.Write("*");
                    }
                    Console.WriteLine(); 
                }
                Console.ReadLine();
            }
            static void soal6()
            {
                int i;
                Console.WriteLine("==== SOAL 6 huruf bintang bintang ====");
                Console.Write("Masukkan kalimat : ");
                char[] kal  = Console.ReadLine().ToLower().Replace(" ","").ToCharArray();
                 
                for (i=0; i < kal.Length; i++)
                {
                    Console.WriteLine($"***{kal[i]}***");
                }
            }
            static void soal5()
            {

                int vokal = 0, konsonan = 0;
                string kal = "aiueoAIUEO";
                Console.WriteLine("==== SOAL 5 SORTIR HURUF VOKAL DAN KONSONAN ====");
                Console.Write("Masukkan kalimat : ");
                string kalimat = Console.ReadLine();

                for (int i = 0; i < kalimat.Length; i++)
                {
                    if (char.IsLetter(kalimat[i]))
                    {
                        if (kal.Contains(kalimat[i]))
                        {
                            vokal += 1;
                        }
                        else
                        {
                            konsonan += 1;
                        }
                    }
                }

                Console.WriteLine($"Jumlah Huruf Vokal    = {vokal}");
                Console.WriteLine($"Jumlah Huruf Konsonan = {konsonan}");

            }
            static void soal3()
            {
                int hariDenda, denda;

                Console.WriteLine("==== SOAL 3 PINJAM BUKU ====");
                Console.Write("Masukkan tanggal Pinjam (dd/mm/yyyy) : ");
                string pinjam = Console.ReadLine();
                Console.Write("Masukkan tanggal Kembali (dd/mm/yyyy) : ");
                string kembali = Console.ReadLine();

                DateTime tglPinjam = DateTime.Parse(pinjam);
                DateTime tglKembali = DateTime.Parse(kembali);

                int hariPinjam = tglKembali.Day - tglPinjam.Day;
                int bulanPinjam = tglKembali.Month - tglPinjam.Month;
                int tahunPinjam = tglKembali.Year - tglPinjam.Year;


                if (tglPinjam.Day < tglKembali.Day || tglPinjam.Month < tglKembali.Month || tglPinjam.Year < tglKembali.Year)
                {

                    if (hariPinjam >= 3 || bulanPinjam >= 1 || tahunPinjam >= 1)
                    {
                        hariDenda = hariPinjam - 3 + (bulanPinjam * 30) + (tahunPinjam * 365);
                        denda = hariDenda * 500;
                        Console.WriteLine($"Anda dikenakan denda sebesar {denda} karena telat mengembalikan buku sebanyak {hariDenda} hari");
                    }
                    else
                    {
                        Console.WriteLine("Anda mengembalikan buku tepat waktu");
                    }
                }
                else
                {
                    Console.WriteLine("Masukkan tanggal, bulan dan tahun dengan benar!!!");
                }

            }
            static void soal4()
            {
                Console.WriteLine("==== SOAL 4 WAKTU ====");
                Console.Write("Masukkan tanggal (dd/mm/yyy) : ");
                DateTime dateStart = DateTime.Parse(Console.ReadLine());

                Console.Write("Berapa lama menuju ujian :");
                int exam = int.Parse(Console.ReadLine());

                Console.Write("Masukkan hari libur : ");
                int[] holiday = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

                DateTime tanggalSelesai = dateStart;

                for( int i = 0; i < exam; i++ )
                {
                     if (i > 0)
                    {
                    tanggalSelesai = tanggalSelesai.AddDays(1);
                    }
                    if (tanggalSelesai.DayOfWeek == DayOfWeek.Saturday)
                    {
                        tanggalSelesai = tanggalSelesai.AddDays(2);
                    }

                for (int j = 0; j < holiday.Length; j++)
                {
                    if (tanggalSelesai.Day == holiday[j])
                    {
                        tanggalSelesai = tanggalSelesai.AddDays(1);
                        if (tanggalSelesai.DayOfWeek == DayOfWeek.Saturday)
                        {
                            tanggalSelesai = tanggalSelesai.AddDays(2);
                        }
                    }
                }
                }
                if (tanggalSelesai.DayOfWeek == DayOfWeek.Sunday)
                {
                    tanggalSelesai = tanggalSelesai.AddDays(2);
                }
              
                    DateTime tanggalUjian = tanggalSelesai.AddDays(1);
                    if (tanggalUjian.DayOfWeek == DayOfWeek.Saturday)
                    {
                        tanggalUjian = tanggalUjian.AddDays(2);
                    }
                    Console.WriteLine();
                    Console.WriteLine("Ujian akan dilaksanakan pada tanggal :" + tanggalUjian.ToString("dddd, dd/MMMM/yyyy"));
            }
            static void soal2_SOS()
            {
                Console.WriteLine("==== SOAL 2 CARI KATA SOS====");
                Console.Write("masukkan kalimat : ");
                char[] kal = Console.ReadLine().ToUpper().ToCharArray();

                int count = 0;
                int count2 = 0;
                string kata ="";


            if (kal.Length % 3 != 0)
            {
                Console.WriteLine("Invalid! masukkan kode yang benar... ");
            }
            else
            {
                for (int i = 0; i < kal.Length; i += 3)
                {
                    if (kal[i] != 'S' || kal[i + 1] != 'O' || kal[i + 2] != 'S')
                    {
                        count++;
                        kata += "SOS";
                    }
                    else
                    {
                        kata += kal[i].ToString() + kal[i+1].ToString() + kal[i+2].ToString();
                        count2++;
                    }
                }
                Console.WriteLine();
                Console.WriteLine($"Total kata yang salah : {count}");
                Console.WriteLine($"Total kata yang benar : {count2}");
                Console.WriteLine($"Kata yang diterima    : {String.Join("", kal)}");
                Console.WriteLine($"Kata yang benar       : {kata}");
            }
            }
            static void soal1()
            {
                double hasil;
                Console.WriteLine("==== SOAL 1 FAKTORIAL ====");
                Console.Write("masukkan bilangan : ");
                int bilangan = int.Parse(Console.ReadLine());
                // string angka ="";

                hasil = 1;
                Console.Write(bilangan + "!=");

                

                for (int i = 1; i <= bilangan; i++)
                {
                    hasil = hasil * i;
                    //angka += angka == ""? i.ToString() : " * " + i.ToString();

                    Console.Write(bilangan - (i - 1));
                    if (i != bilangan)
                    
                        Console.Write("*");
                }
                Console.WriteLine();
                Console.Write(bilangan + "!");
                Console.Write("=");

                Console.Write(hasil);
                Console.WriteLine($"Ada {hasil} cara");
                Console.ReadLine();
          
                int faktorial = 1;
                for (int i = 1; i <= 5; i++)
                {
                    faktorial = faktorial * i;
                }
                Console.Write("Faktorial = " + faktorial);
                Console.Read();
            }    
    }
}



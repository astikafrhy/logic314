﻿using System;
using System.Collections.Generic;

namespace Logic06
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //list();
            //contohClass();
            listClass();
            
            

            Console.ReadKey();
        }

        static void listRemove()
        {
            List<User> listUser = new List<User>()
            {
                new User(){Name = "Isni Dwitiniardi", Age = 22},
                new User(){Name = "Astika Fujianti Rahayu", Age = 23},
                new User(){Name = "Alfi", Age = 23}
            };
            listUser.Add(new User() { Name = "Anwar", Age = 24 });
            listUser.RemoveAt(2);

            for (int i = 0; i < listUser.Count; i++)
            {
                Console.WriteLine(listUser[i].Name + ", berumur" + listUser[i].Age + " tahun");
            }
        }

        static void listAdd()
        {
            List<User> listUser = new List<User>()
            {
                new User(){Name = "Isni Dwitiniardi", Age = 22},
                new User(){Name = "Astika Fujianti Rahayu", Age = 23},
                new User(){Name = "Alfi", Age = 23}
            };
            listUser.Add(new User() { Name = "Anwar", Age = 24 });

            for (int i = 0; i < listUser.Count; i++)
            {
                Console.WriteLine(listUser[i].Name + ", berumur" + listUser[i].Age + " tahun");
            }

        }
        static void listClass()
        {
            List<User> listUser = new List<User>()
            {
                new User(){Name = "Isni Dwitiniardi", Age = 22},
                new User(){Name = "Astika Fujianti Rahayu", Age = 23},
                new User(){Name = "Alfi", Age = 23}
            };

            for(int i = 0; i < listUser.Count; i++)
            {
                Console.WriteLine(listUser[i].Name+ ", berumur" + listUser[i].Age + " tahun");
            }  
        }
        static void contohClass()
        {

            Mobil mobil = new Mobil("RI 1");

            mobil.percepat();
            mobil.maju();
            mobil.isiBensin(12);

            string platno = mobil.getPlatno();

            Console.WriteLine($"Plat Nomor : {platno}");
            Console.WriteLine($"Bensin     : {mobil.bensin}");
            Console.WriteLine($"Kecepatan  : {mobil.kecepatan}");
            Console.WriteLine($"Posisi     : {mobil.posisi}");
        }
        static void list()
        {
            Console.WriteLine("==== LIST ====");
            Console.Write("Nama : ");

            List<string> list = new List<string>()
            {
               "\n" + "Astika",
                "isni",
                "firda"
            };

            Console.WriteLine(String.Join(",\n" , list));
        }
    }
}

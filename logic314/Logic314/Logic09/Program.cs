﻿using System;

namespace Logic09
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soal1();
            //soal2();
            //soal3();
            soal3_dua();
           
        }
        static void soal1()
        {
            int plhCustomer;
            double jarak = 2;
            double jarak2 = 0.5;
            double jarak3 = 1.5;
            double jarak4 = 0.3;
            int bensin;
            double hasil;
            Console.WriteLine("==== SOAL 1 SUPIR OJOL =====");

            Console.Write("Masukkan pilihan: ");
            plhCustomer = int.Parse(Console.ReadLine());

            switch (plhCustomer)
            {
                case 1:
                    {
                        bensin = 1;
                        hasil = jarak;
                        Console.WriteLine("Jarak Tempuh = " + jarak + "KM");
                        Console.WriteLine($"bensin yang habis digunakan = {bensin} liter");
                        break;
                    }
                case 2:
                    {
                        bensin = 1;
                        hasil = jarak + jarak2;
                        Console.WriteLine("Jarak Tempuh = " + (jarak + "KM"+ " + " + jarak2+"KM")+ " = " + hasil + "KM");
                        Console.WriteLine($"bensin yang habis digunakan = {bensin} liter");
                        break;
                    }
                case 3:
                    {
                        bensin = 2;
                        hasil = jarak + jarak2 + jarak3;
                        Console.WriteLine("Jarak Tempuh = " + (jarak+"KM" + " + " + jarak2+"KM" +" + "+ jarak3+"KM")+ " = " + hasil + "KM");
                        Console.WriteLine($"bensin yang habis digunakan = {bensin} liter");
                        break;
                    }
                case 4:
                    {
                        bensin = 2;
                        hasil = jarak + jarak2 + jarak3 + jarak4;
                        Console.WriteLine("Jarak Tempuh = " + (jarak+"KM" + " + " + jarak2+"KM"+ " + " + jarak3+"KM"+ " + " + jarak4+"KM")+ " = " + hasil + "KM");
                        Console.WriteLine($"bensin yang habis digunakan = {bensin} liter");
                        break;
                    }
                default:
                    {
                        Console.WriteLine("=========================");
                        Console.WriteLine("Customer tidak tersedia!");
                        break;
                    }
            }
        }

        static void soal2()
        {
            double terigu = 115;
            double gulapasir = 190;
            double susu = 100;
            
            Console.WriteLine("===== SOAL 2 RESEP KUE PUKIS ====");
            Console.Write("Berapa banyak kue pukis yang akan dibuat : ");
            int pukis = int.Parse(Console.ReadLine());
 
            Console.WriteLine("Terigu = " + Math.Round(terigu/15 * pukis)/*,MidpointRounding.ToPositiveInfinity)*/ + "gr");
            Console.WriteLine("Gula pasir = " + Math.Round(gulapasir/15 * pukis) + "gr");
            Console.WriteLine("Susu = " + Math.Round(susu/15 * pukis) + "ml");
        }

        static void soal3()
        {
            Console.WriteLine("==== SOAL 3 ====");
            Console.Write("Masukkan indeks (N)           : ");
            int indeks = int.Parse(Console.ReadLine());
            Console.Write("Masukkan banyak kelipatan (O) : ");
            int  n = int.Parse(Console.ReadLine());

            int tampung = 0;
            for (int i = 0; i < indeks; i++)
            {
                for(int j = 0; j < indeks; j++)
                {
                    if(j == 0 && i >= 0 && i < indeks)
                    {
                        tampung = n;
                        Console.Write(tampung + "\t");
                    }
                    else if (i == indeks - 1)
                    {
                        tampung += n;
                        Console.Write(tampung + "\t");
                    }
                    else if (j == i)
                    {
                        tampung += n *i;
                        Console.Write(tampung + "\t");
                    }
                    else
                    {
                        Console.Write("\t");
                    }

                }
                    Console.WriteLine();
            }
        }

        static void soal3_dua()
        {
            Console.WriteLine("==== SOAL 3 ====");
            Console.Write("Masukkan indeks (N)           : ");
            int indeks = int.Parse(Console.ReadLine());
            Console.Write("Masukkan banyak kelipatan (O) : ");
            int angka = int.Parse(Console.ReadLine());
            int hasil = angka;

            for(int i = 0; i < indeks; i++)
            {
                int x = 0;
                for(int j=0; j < indeks; j++)
                {
                    x += angka;
                    if (j == 0 || i == indeks - 1 || i == j)
                    {
                        Console.Write(x.ToString() + "\t");
                    }
                    else
                        Console.Write("\t");
                }
                Console.WriteLine();
            }
        }
    }
}

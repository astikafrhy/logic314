-- SQL DAY 02 --

--cast
select cast(10 as decimal(18,4))
select cast('1000' as int)
select cast (10.65 as int)
select cast('2000/03/16' as date)

--convert
select CONVERT(decimal(18,4) , 10) as to_decimal
select CONVERT(int , '100') as to_integer
select CONVERT(int , 10.65) as to_integer
select CONVERT(date , '2000/03/16') as to_date

--getdate
select getdate() as tanggal, GETUTCDATE()
select day(GETDATE()) AS HARI , MONTH(GETDATE()) AS BULAN, YEAR(GETdATE()) AS TAHUN

--dateadd
select dateadd(year,2,getdate()) , dateadd(month,3,getdate()), dateadd(day,5,getdate())

--datediff
select datediff(year, '2023/03/16' , '2030/03/16')
select datediff(month, '2023/03/16' , '2023/06/16')
select datediff(day, '2023/03/16' , '2023/03/20')

--sub query
select nama,alamat,email,panjang from mahasiswa
where panjang = (select max(panjang) from mahasiswa)

insert into [dbo].[mahasiswa]
select nama, alamat, email, panjang from mahasiswa

select*from mahasiswa
delete from mahasiswa where id <=6
delete from mahasiswa where id =8

--view sql
create view vwMahasiswa2
as
select * from mahasiswa

select * from vwMahasiswa2

drop view vwMahasiswa2

--database index
create index index_nama
on mahasiswa(nama)

create index index_alamat_email
on mahasiswa(alamat,email)

--unique index
create unique index index_panjang
on mahasiswa(panjang)

--drop index & unique index
drop index index_alamat_email on mahasiswa
drop index index_panjang on mahasiswa

-- add primary key
alter table mahasiswa
add constraint pk_id primary key(id,alamat)

--drop primary key
alter table mahasiswa drop constraint pk_id

--add unique constraint 
alter table mahasiswa add constraint unique_alamat unique(alamat)
alter table mahasiswa add constraint unique_panjang unique(panjang)

update mahasiswa set panjang=49 where id=12
select * from mahasiswa

--drop unique constraint 
alter table mahasiswa drop constraint unique_alamat
